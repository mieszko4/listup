(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listupFilters', ['ngResource']);

	angular.module('listupFilters').filter('encodeURIComponent', function ($window) {
		return $window.encodeURIComponent;
	});

	angular.module('listupFilters').filter('songTime', function () {
		return function (time) {
			var sec_num = parseInt(time, 10),
				hours   = Math.floor(sec_num / 3600),
				minutes = Math.floor((sec_num - (hours * 3600)) / 60),
				seconds = sec_num - (hours * 3600) - (minutes * 60);

			if (hours === 0) {
				hours = '';
			} else if (hours < 10) {
				hours = '0' + hours + ':';
			} else {
				hours = hours + ':';
			}
			if (minutes < 10) {
				minutes = '0' + minutes + ':';
			} else {
				minutes = minutes + ':';
			}
			if (seconds < 10) {
				seconds = '0' + seconds;
			}

			return hours + minutes + seconds;
		};
	});
}(angular));