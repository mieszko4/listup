(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('facebookLogin', []).run(function ($location, LogService) {
		var appId = '175972395921488'; //TODO: get from config

		console.log('loading facebook login module');
		window.fbAsyncInit = function () {
			window.FB.init({
				'appId'    : appId,
				channelUrl : '//listup.audio:' + $location.port() + '/external/channel.html',
				status     : true, // check login status
				cookie     : true, // enable cookies to allow the server to access the session
				xfbml      : true  // parse XFBML
			});
		};

		window.fbSignInCallback = function () {
			if (!window.FB) {
				return;
			}

			//the response could be cached/invalid
			window.FB.getLoginStatus(function (response) {
				if (response.status === 'connected') {
					LogService.login('facebook'); //check on server if not valid
					return;
				}

				window.FB.login(function (response) {
					if (response.authResponse) {
						LogService.login('facebook');
					} else {
						//ignore
					}
				});
			});
		};

		// Load the SDK asynchronously
		(function (d) {
			var js,
				id = 'facebook-jssdk',
				ref = d.getElementsByTagName('script')[0];

			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement('script');
			js.id = id;
			js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			ref.parentNode.insertBefore(js, ref);
		}(document));
	});
}(angular));
