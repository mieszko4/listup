(function (angular, $) {
	"use strict";
	/*global angular, console, jQuery*/

	angular.module('listupServices', ['ngResource']);

	angular.module('listupServices').factory('Playlist', function ($resource) {
		return $resource('../api/1.0/playlists/:playlistId', {},
			{
				update: {method: 'PUT'}
			});
	});

	angular.module('listupServices').factory('User', function ($resource) {
		return $resource('../api/1.0/users/:userId', {},
			{
			});
	});

	angular.module('listupServices').factory('ListUp', function ($http, UserPreferencesService, $timeout, $window, $document, $location) {
		var volume = UserPreferencesService.get('global-volume'),
			repeatPlaylist = UserPreferencesService.get('repeat-playlist'),
			ListUp = {
				userId: undefined,
				type: undefined,
				username: undefined,
				avatarUrl: undefined,
				loggedIn: true,
				playing: false,
				pausing: false,
				playingPlaylist: undefined,
				playingIndex: undefined,
				playingTitle: '',
				songObject: undefined,
				'volume': (volume !== undefined && volume !== null) ? parseInt(volume, 10) : 50,
				globalCreatePlaylist: {
					songsOptions: [],
					playlist: {
						songs: [],
						vote: 0,
						suggest: 0,
						visibility: 'all'
					}
				},
				repeatPlaylist: (repeatPlaylist !== undefined && repeatPlaylist !== null) ? (repeatPlaylist === 'true') : true,
				songOptionsNumber: 5,
				repaintScreen: function () {
					$timeout(function () { //TODO: is there a better way?
						$($window).trigger('resize');
					});
				},
				scrollToAddedSong: function ($element) {
					$timeout(function () { //TODO: do it better way
						var $item = $($element).find('.songs-chooser .newSongsDiv:last');
						if ($item.length === 1) {
							ListUp.scrollToElement.call(this, $item);
						}
					});
				},
				scrollToElement: function ($element) {
					var padding = $('.ui-page-active>.ui-header:first').outerHeight();
					$('html, body').animate({
						scrollTop: ($element.offset().top - padding) + 'px'
					}, 'fast');
				},
				scrollToSong: function ($element, index) {
					$timeout(function () { //TODO: do it better way
						var $li = $($element).find('.listBannerBefore li').eq(index);
						if ($li.length === 1) {
							ListUp.scrollToElement.call(this, $li);
						}
					});
				},
				closeOtherCollapsibles: function (selector, exceptIndex) {
					var isCollapsed = $(selector).eq(exceptIndex).find('.ui-collapsible.ui-collapsible-collapsed').length === 1;

					$(selector).eq(exceptIndex).siblings().find('.ui-collapsible').trigger('collapse'); //TODO: put in directive

					if (isCollapsed) { //it was collapsed
						ListUp.scrollToElement.call(this, $(selector).eq(exceptIndex));
					}
				},
				initCollapsibleClick: function ($element, selector) {
					$($element).delegate(selector, 'click', function () {
						$(this).parents('.ui-collapsible:first').trigger('collapse');
					});
				},
				fixNavBarSelection: function ($element) {
					$($element).find('.default-navbar-selection').addClass('ui-btn-active');
				},
				inArray: function (element, array) {
					return $.inArray.call(this, element, array);
				},
				rememberAndGo: function (location) {
					ListUp.returnPath = $location.path();
					ListUp.returnUrl = $location.url();
					$location.path(location);
				},
				applyNewOrder: function (array, newPositions) {
					var newArray = [];

					angular.forEach(newPositions, function (newPosition) {
						newArray.push(array[newPosition]);
					});

					return newArray;
				},
				calculatePlaylistsInitialLimit: function () {
					var headerHeight = $('.ui-page-active>.ui-header:first').outerHeight(),
						footerHeight = $('.ui-page-active>.ui-footer:first').outerHeight(),
						windowHeight = $('.ui-page-active').outerHeight(),
						leftHeight = windowHeight - (headerHeight + footerHeight),
						heightOfLi = 80; //this is approximate down height of list item

					return Math.ceil(leftHeight / heightOfLi);
				},
				virtualScrollLimit: 10
			};

		$http.get('../api/1.0/login').success(function (data) {
			ListUp.userId = data.userId;
			ListUp.type = data.type;
			ListUp.username = data.username;
			ListUp.avatarUrl = data.avatarUrl || '../css/user-icon.png';
			ListUp.loggedIn = data.loggedIn;
		});

		return ListUp;
	});

	angular.module('listupServices').factory('SongService', function ($q, $rootScope, $window) {
		//TODO: notify user that the service is not available
		if ($window.SC !== undefined) {
			$window.SC.initialize({
				client_id: '6e17ff36dca5958058cb780d8d41cc99'
			});

			//TODO: for mobiles so that autoStart works.
			var tmpSCid = 94002701;
			$window.SC.stream(
				"/tracks/" + tmpSCid,
				{
					multiShotEvents: true,
					preferFlash: false,
					useHTML5Audio: true
				},
				function () { }
			);
		} else {
			$window.SC = {};
		}
		return {
			getSongDetails: function (searchTerm) {
				var deferred = $q.defer();
				$window.SC.get(
					'/tracks',
					{
						q: searchTerm,
						limit: 5,
						filter: 'streamable'
					},
					function (tracks) {
						$rootScope.$apply(function () {
							deferred.resolve(tracks);
						});
					}
				);
				return deferred.promise;
			}
		};
	});

	angular.module('listupServices').factory('LogService', function ($http, $q, authService, ListUp, $window) {
		return {
			login: function (type, username, password) {
				var deferred = $q.defer(),
					params;
				if (password !== undefined) {
					params = {
						"type": type,
						"username": username,
						"password": password
					};
				} else {
					params = {
						"type": type,
						"access_token": username
					};
				}

				$http.post('../api/1.0/login', params).success(function (data) {
					deferred.resolve(true);
					authService.loginConfirmed();
					ListUp.userId = data.userID;
					ListUp.type = data.type;
					ListUp.username = data.username;
					ListUp.avatarUrl = data.avatarUrl || '../css/user-icon.png';
					ListUp.loggedIn = true;
				}).error(function (data, status) {
					ListUp.userId = undefined;
					ListUp.type = undefined;
					ListUp.username = undefined;
					ListUp.avatarUrl = undefined;
					ListUp.loggedIn = false;
					deferred.resolve(false);
					console.error("Login failed");

					if (params.type === 'facebook' && status === 403) {
						//if response not valid, refresh cache and display info to user
						$window.FB.getLoginStatus(function () {
							console.warn('Please try to log in again');
							$(".facebook-popup").popup("open"); //TODO: nicer way
						}, true);
					}
				});
				return deferred.promise;
			},
			logout: function () {
				var deferred = $q.defer();
				$http.post('../api/1.0/logout').success(function () {
					if (ListUp.type === 'facebook') {
						console.log('Logout from FB account', 'FB policy');
						$window.FB.getLoginStatus(function (response) {
							if (response.status === 'connected') {
								$window.FB.logout();
							}
						}, true);
					} else if (ListUp.type === 'google+') {
						//ignore
					}

					ListUp.userId = undefined;
					ListUp.type = undefined;
					ListUp.avatarUrl = undefined;
					ListUp.username = undefined;
					ListUp.loggedIn = false;
					deferred.resolve(true);
					console.log('User is logged out');
				});
			}
		};
	});

	angular.module('listupServices').factory('UserPreferencesService', function (FeatureChecker, $window) {
		return {
			set: function (key, value) {
				if (FeatureChecker.localStorage()) {
					$window.localStorage.setItem(key, value);
					return true;
				} else {
					return false;
				}
			},
			get: function (key) {
				if (FeatureChecker.localStorage()) {
					return $window.localStorage.getItem(key);
				} else {
					return undefined;
				}
			}
		};
	});

	angular.module('listupServices').factory('FeatureChecker', function ($window, $document) {
		return {
			cookies: function () {
				var returnValue;

				if ($window.navigator.cookieEnabled) {
					returnValue = true;
				} else {
					$document[0].cookie = "cookietest=1";
					returnValue = $document[0].cookie.indexOf("cookietest=") !== -1;
					$document[0].cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";
				}

				if (!returnValue) {
					console.warn('Cookies are disabled');
				}

				return returnValue;
			},
			localStorage: function () {
				var returnValue = false;

				try {
					returnValue = ($window.localStorage !== undefined);
					if (!returnValue) {
						console.warn('No support for localStorage');
					}
				} catch (e) {
					console.warn('User agent policy blocks localStorage');
				}

				return returnValue;
			}
		};
	});
}(angular, jQuery));
