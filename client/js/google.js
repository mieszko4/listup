(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('googleLogin', []).run(function (LogService) {
		var clientId = '163503195549.apps.googleusercontent.com'; //TODO: get from config

		console.log('loading google+ login module');

		window.googlePlusAsyncInit = function () {
			window.gapi.auth.init();
		};

		window.googlePlusSignInCallback = function () {
			if (!window.gapi) {
				return;
			}

			window.gapi.auth.authorize({
				'client_id': clientId,
				'scope': 'https://www.googleapis.com/auth/plus.login'
			}, function (response) {
				if (response && response.access_token) {
					console.log('log g+');
					LogService.login('google+', response.access_token);
				} else {
					//ignore
				}
			});
		};

		(function () {
			var po = document.createElement('script'),
				s;
			po.type = 'text/javascript';
			po.async = true;
			po.src = 'https://apis.google.com/js/client:plusone.js?onload=googlePlusAsyncInit';

			s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(po, s);
		}());
	});
}(angular));