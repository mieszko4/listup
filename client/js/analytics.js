(function (angular, window, document, $) {
	"use strict";
	/*global angular, window, console, document, jQuery*/
	/*jslint nomen: true*/

	window._gaq = window._gaq || [];

	angular.module('listupAnalytics', []).run(function ($location, $routeParams) {
		var ga,
			s,
			convertPathToQueryString;

		window._gaq.push(['_setAccount', 'UA-40867262-1']);
		window._gaq.push(['_trackPageview']);

		ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

		s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);

		$(document).bind('pageshow', function () {
			var path = convertPathToQueryString($location.path(), $routeParams);
			window._gaq.push(['_trackPageview', path]);
		});

		convertPathToQueryString = function (path, $routeParams) {
			var key,
				queryParam,
				querystring;

			for (key in $routeParams) {
				if ($routeParams.hasOwnProperty(key)) {
					queryParam = '/' + $routeParams[key];
					path = path.replace(queryParam, '');
				}
			}

			querystring = decodeURIComponent($.param($routeParams));
			if (querystring === '') {
				return path;
			}
			return path + "?" + querystring;
		};
	});
}(angular, window, document, jQuery));