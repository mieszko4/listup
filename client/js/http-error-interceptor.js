(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('http-error-interceptor', []).config(['$httpProvider', function ($httpProvider) {
		var interceptor = ['$rootScope', '$q', function ($rootScope, $q) {
			function success(response) {
				return response;
			}

			function error(response) {
				if (response.status === 404) {
					var deferred = $q.defer();
					$rootScope.$broadcast('event:404-not-found');
					return deferred.promise;
				}
				// otherwise, default behaviour
				return $q.reject(response);
			}

			return function (promise) {
				return promise.then(success, error);
			};

		}];
		$httpProvider.responseInterceptors.push(interceptor);
	}]);
}(angular));