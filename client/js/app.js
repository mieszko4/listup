(function (angular, window, $) {
	"use strict";
	/*global angular, window, console, jQuery*/

	// Declare app level module which depends on filters, and services
	angular.module('listup', ['listupServices', 'listupFilters', 'listupDirectives', 'listupAnalytics', 'facebookLogin', 'googleLogin']);

	angular.module('listup').config(function ($routeProvider) {
		$routeProvider.
			when('/playlists', {
				templateUrl: 'partials/playlists-list.html',
				onActivate: 'onActivate("p")'
			}).when('/playlists/show/:playlistId', {
				templateUrl: 'partials/playlist-details.html',
				onActivate: 'onActivate("s", playlistId)'
			}).when('/playlists/edit/:playlistId', {
				templateUrl: 'partials/playlist-save.html',
				onActivate: 'onActivate("e", playlistId)'
			}).when('/playlists/suggest/:playlistId', {
				templateUrl: 'partials/playlist-suggest.html',
				onActivate: 'onActivate("g", playlistId)'
			}).when('/user/:userId/playlists', {
				templateUrl: 'partials/user-playlists-list.html',
				onActivate: 'onActivate("m")'
			}).when('/create', {
				templateUrl: 'partials/playlist-save.html',
				onActivate: 'onActivate("c")'
			}).when('/login', {
				templateUrl: 'partials/login.html',
				onActivate: 'onActivate()'
			}).when('/signup', {
				templateUrl: 'partials/signup.html',
				onActivate: 'onActivate()'
			}).when('/enable-cookies', {
				templateUrl: 'partials/enable-cookies.html',
				onActivate: 'onActivate()'
			}).when('/signup-success', {
				templateUrl: 'partials/signup-success.html',
				onActivate: 'onActivate()'
			}).when('/not-found', {
				templateUrl: 'partials/404.html',
				onActivate: 'onActivate()'
			}).when('/feedback', {
				templateUrl: 'partials/feedback.html',
				onActivate: 'onActivate()'
			}).otherwise({
				redirectTo: '/playlists'
			});
	});
	angular.module('listup').config(function ($locationProvider) {
		$locationProvider.html5Mode(false);  //TODO: remove this line and change nginx configuration
	});

	angular.module('listup').run(function ($templateCache) {
		$.mobile.page.prototype.options.domCache = true;
	});

	if (window.console === undefined) {
		window.console = {};
		window.console.warn = function () {};
		window.console.info = function () {};
		window.console.error = function () {};
		window.console.log = function () {};
	}
}(angular, window, jQuery));