(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listupDirectives', ['http-auth-interceptor', 'http-error-interceptor', 'ui.sortable', 'infinite-scroll']);

	angular.module('listupDirectives').directive('authListup', function ($location) {
		var previousPath;
		return {
			restrict: 'C',
			link: function (scope) {
				scope.$on('event:auth-loginRequired', function () {
					previousPath = $location.path();
					$location.path('/login');
				});

				scope.$on('event:auth-loginConfirmed', function () {
					if (previousPath !== undefined && previousPath !== '/login') { //ako ne znamo prijašnju lokaciju i ako je ona različita od logina
						$location.path(previousPath);
					} else {
						$location.path('/');
					}
				});
			}
		};
	});

	angular.module('listupDirectives').directive('errorListup', function ($location) {
		return {
			restrict: 'C',
			link: function (scope) {
				scope.$on('event:404-not-found', function () {
					var previousLocation = $location.path();
					$location.path('/not-found').search({url: previousLocation});
					$location.replace();
				});
			}
		};
	});


	angular.module('listupDirectives').directive('passwordValidator', [function () {
		return {
			require: 'ngModel',
			link: function (scope, elm, attr, ctrl) {
				var pwdWidget = elm.inheritedData('$formController')[attr.passwordValidator];

				ctrl.$parsers.push(function (value) {
					if (value === pwdWidget.$viewValue) {
						ctrl.$setValidity('MATCH', true);
						return value;
					}
					ctrl.$setValidity('MATCH', false);
				});

				pwdWidget.$parsers.push(function (value) {
					ctrl.$setValidity('MATCH', value === ctrl.$viewValue);
					return value;
				});
			}
		};
	}]);

	angular.module('listupDirectives').directive('stopEvent', function () {
		return {
			restrict: 'A',
			link: function (scope, element, attr) {
				element.bind(attr.stopEvent, function (e) {
					e.stopPropagation();
					e.preventDefault();
				});
			}
		};
	});
}(angular));