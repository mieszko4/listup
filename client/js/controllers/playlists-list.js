(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('PlaylistListCtrl', function ($scope, Playlist, ListUp, $timeout) {
		var limit,
			loaded;
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);

			loaded = 0;
			$scope.disableScroll = false;
			$scope.allLoaded = false;
			$scope.playlists = [];
			$timeout(function () { //TODO: other way
				limit = ListUp.calculatePlaylistsInitialLimit();
				$scope.addMore();
				limit = ListUp.virtualScrollLimit;
			});
		};

		$scope.$on('$routeChangeStart', function() {
			$scope.disableScroll = true;
		});

		$scope.addMore = function () {
			if (!$scope.allLoaded && !$scope.busy && limit !== undefined) {
				$scope.busy = true;
				Playlist.query({
					'skip': loaded,
					'limit': limit,
					'orderBy': 'time',
					'desc': 'true',
					'visibility': 'all'
				}, function (playlists) {
					angular.forEach(playlists, function (playlist) {
						$scope.playlists.push(playlist);
					});
					loaded += playlists.length;
					$scope.busy = false;

					if (playlists.length === 0) {
						$scope.allLoaded = true;
					}
				});
			}
		};
	});
}(angular));