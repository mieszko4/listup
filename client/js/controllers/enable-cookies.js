(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('EnableCookiesCtrl', function ($scope, FeatureChecker, $location, ListUp) {
		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			$scope.reload();
		};

		$scope.reload = function () {
			if (FeatureChecker.cookies()) {
				if (ListUp.returnPath !== undefined) {
					$location.path(ListUp.returnPath);
					ListUp.returnPath = undefined;
				} else {
					$location.path('/');
				}
			}
		};
	});
}(angular));
