(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('SignUpCtrl', function ($scope, $http, $location, ListUp, FeatureChecker, $window) {
		$scope.user = {};
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.skipReload) {
				ListUp.skipReload = undefined;
				return;
			}

			$scope.showError = false;
			$scope.showConflictError = false;
			$scope.submitted = false;
			$scope.user.password = '';
			$scope.passwordConfirm = '';

			if (ListUp.loggedIn) {
				$location.path('/');
				return;
			}

			if (!FeatureChecker.cookies()) {
				ListUp.returnPath = $location.path();
				$location.path('/enable-cookies');
				return;
			}
		};

		$scope.facebookLogin = function () {
			$window.fbSignInCallback.call(this);
		};

		$scope.googleLogin = function () {
			$window.googlePlusSignInCallback.call(this);
		};

		$scope.signUp = function () {
			$scope.submitted = true;
			if ($scope.user.password === $scope.passwordConfirm) {
				$scope.data = angular.toJson($scope.user);
				$http.post('../api/1.0/users', $scope.data).success(function () {
					console.log('User is registered');
					$location.path('/signup-success');
					$scope.showError = false;
					$scope.showConflictError = false;
				}).error(function (data, status) {
					if (status === 409) {
						$scope.showConflictError = true;
					} else {
						$scope.showError = true;
					}
				});
			} else {
				$scope.showError = true;
			}
		};
	});
}(angular));