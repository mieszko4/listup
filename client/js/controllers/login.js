(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('LoginCtrl', function ($scope, $location, LogService, ListUp, FeatureChecker, $window) {
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.skipReload) {
				ListUp.skipReload = undefined;
				return;
			}

			$scope.showError = false;
			$scope.submitted = false;
			$scope.password = '';

			if (ListUp.loggedIn) {
				$location.path('/');
				return;
			}

			if (!FeatureChecker.cookies()) {
				ListUp.returnPath = $location.path();
				$location.path('/enable-cookies');
				return;
			}
		};

		$scope.facebookLogin = function () {
			$window.fbSignInCallback.call(this);
		};

		$scope.googleLogin = function () {
			$window.googlePlusSignInCallback.call(this);
		};

		$scope.internalLogin = function () {
			$scope.submitted = true;
			LogService.login('internal', $scope.username, $scope.password).then(function (success) {
				$scope.showError = !success;
			});
		};
	});
}(angular));
