(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('SignUpSuccessCtrl', function ($scope, $location, ListUp) {
		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.loggedIn) {
				$location.path('/');
				return;
			}
		};
	});
}(angular));
