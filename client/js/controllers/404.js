(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('NotFoundCtrl', function ($scope, ListUp, $routeParams) {
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			$scope.url = $routeParams.url;
			ListUp.repaintScreen($scope);
		};
	});

}(angular));