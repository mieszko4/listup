(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('PlaylistSuggestCtrl', function ($scope, $routeParams, $location, $http, Playlist, SongService, ListUp, $element) {
		$scope.ListUp = ListUp;
		$scope.sortableOptions = {
			stop: function () {
				var fixedSongsOptions = [];

				//TODO: hack because of usage of ngm-swipe
				angular.forEach($scope.songsOptions, function (songOption) {
					if (songOption !== undefined) {
						fixedSongsOptions.push(songOption);
					}
				});
				$scope.songsOptions = fixedSongsOptions;
				$scope.$apply();

				//update playlist.songs
				$scope.playlist.songs = [];
				angular.forEach($scope.songsOptions.length, function (songsOption) {
					$scope.playlist.songs.push(songsOption.songs[songsOption.chosen]);
				});
			},
			handle: '.ui-li-thumb'
		};

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.skipReload) {
				ListUp.skipReload = undefined;
				return;
			}

			$scope.selectedSong = {};
			$scope.songsOptions = [];
			$scope.currentPlaylistId = $routeParams.playlistId;

			$scope.playlist = {};
			Playlist.get({playlistId: $routeParams.playlistId}, function (playlist) {
				$scope.playlist.title = playlist.title;
				$scope.playlist.songs = [];
				$scope.playlist.vote = 0;
				$scope.playlist.suggest = 0;
			});

			ListUp.initCollapsibleClick($element, '.song-suggestions li'); //TODO: do it angular way
		};

		$scope.closeOtherCollapsibles = function (i) {
			ListUp.closeOtherCollapsibles('.newSongsDiv', i); //TODO: move to directive
		};

		$scope.addSong = function () {
			var arraySongs = [],
				songOption = {};

			$scope.playlist.imgURL = '';
			SongService.getSongDetails($scope.searchTerm).then(function (tracks) {
				if (tracks.length === 0) {
					$scope.notExists = true;
					$scope.searchTerm = '';
				} else {
					$scope.notExists = false;
					if (tracks.length > ListUp.songOptionsNumber) {
						tracks.length = ListUp.songOptionsNumber; //limit
					}
					angular.forEach(tracks, function (track) {
						if (track.artwork_url === null) {
							track.artwork_url = './css/logoPlaylistGrey.png';
						}
						songOption =
							{
								"artist": track.user.username,
								"title": track.title,
								"search_term": $scope.searchTerm !== undefined ? $scope.searchTerm : '',
								"imgURL": track.artwork_url,
								"SCid": track.id,
								"isAccepted": 0,
								"$index": $scope.playlist.songs.length
							};
						arraySongs.push(songOption);
					});
					$scope.songsOptions.push({songs: arraySongs, chosen: 0});
					//add first selection on default
					$scope.playlist.songs.push(arraySongs[0]);
					$scope.playlist.imgURL = $scope.imgURL + arraySongs[0].imgURL;
					$scope.searchTerm = '';

					ListUp.scrollToAddedSong($element); //TODO: do it better way
				}
			});
		};

		$scope.selectSong = function (songIndex, selectedSong, songs) {
			$scope.playlist.imgURL = '';
			$scope.selectedIndex = selectedSong;
			songs.chosen = selectedSong;
			$scope.playlist.songs[songIndex] = $scope.songsOptions[songIndex].songs[selectedSong];
			$scope.playlist.imgURL = $scope.playlist.imgURL + $scope.songsOptions[songIndex].songs[selectedSong].imgURL;
		};

		$scope.sendSuggestions = function () {
			if (($routeParams.playlistId == ListUp.playingPlaylist) && ((ListUp.playing != false) || (ListUp.pausing != false))) {
				$scope.openedSuggestPop = true;
			} else {
				$http.post('../api/1.0/playlists/' + $routeParams.playlistId + '/send_suggestion', angular.toJson($scope.playlist.songs)).success(function () {
					$location.path('/playlists/show/' + $routeParams.playlistId);
				});
			}
		};

		$scope.deleteSong = function (index) {
			$scope.songsOptions.splice(index, 1);
			$scope.playlist.songs.splice(index, 1);
		};
	});
}(angular));