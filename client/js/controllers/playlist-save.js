(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('PlaylistSaveCtrl', function ($scope, $routeParams, $location, Playlist, SongService, ListUp, $element) {
		$scope.ListUp = ListUp;
		$scope.sortableOptions = {
			stop: function () {
				var fixedSongsOptions = [];

				//TODO: hack because of usage of ngm-swipe
				angular.forEach($scope.songsOptions, function (songOption) {
					if (songOption !== undefined) {
						fixedSongsOptions.push(songOption);
					}
				});
				$scope.songsOptions = fixedSongsOptions;
				if ($scope.isNew) {
					ListUp.globalCreatePlaylist.songsOptions = fixedSongsOptions;
				}
				$scope.$apply();

				//update playlist.songs
				$scope.playlist.songs = [];
				angular.forEach($scope.songsOptions, function (songsOption) {
					$scope.playlist.songs.push(songsOption.songs[songsOption.chosen]);
				});
			},
			handle: '.ui-li-thumb'
		};

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.skipReload) {
				ListUp.skipReload = undefined;
				return;
			}

			$scope.selectedSong = {};
			$scope.playlistView = 'create';
			$scope.imgURL = '';

			ListUp.fixNavBarSelection($element); //TODO: bug, find other way
			ListUp.initCollapsibleClick($element, '.song-suggestions li'); //TODO: do it angular way

			if (!$routeParams.playlistId) {
				$scope.isNew = true;

				//double bind it
				$scope.songsOptions = ListUp.globalCreatePlaylist.songsOptions;
				$scope.playlist = ListUp.globalCreatePlaylist.playlist;
			} else { //edit playlist
				$scope.isNew = false;
				$scope.songsOptions = [];

				$scope.playlist = Playlist.get({playlistId: $routeParams.playlistId}, function () {
					angular.forEach($scope.playlist.songs, function (song, i) {
						song.$index = i;
						$scope.songsOptions.push({songs: [song], chosen: 0});
					});
				});
			}

			$scope.saveLabel = $scope.isNew ? 'CREATE' : 'SAVE';
		};

		$scope.closeOtherCollapsibles = function (i) {
			ListUp.closeOtherCollapsibles('.newSongsDiv', i); //TODO: move to directive
		};

		$scope.addSong = function () {
			var arraySongs = [],
				songOption = {};

			$scope.playlist.imgURL = '';
			SongService.getSongDetails($scope.searchTerm).then(function (tracks) {
				if (tracks.length === 0) {
					$scope.notExists = true;
					$scope.searchTerm = '';
				} else {
					$scope.notExists = false;
					if (tracks.length > ListUp.songOptionsNumber) {
						tracks.length = ListUp.songOptionsNumber; //limit
					}
					angular.forEach(tracks, function (track) {
						if (track.artwork_url === null) {
							track.artwork_url = './css/logoPlaylistGrey.png';
						}
						songOption =
							{
								"artist": track.user.username,
								"title": track.title,
								"search_term": $scope.searchTerm !== undefined ? $scope.searchTerm : '',
								"imgURL": track.artwork_url,
								"SCid": track.id,
								"isAccepted": 1,
								"$index": $scope.playlist.songs.length
							};
						arraySongs.push(songOption);
					});

					$scope.songsOptions.push({songs: arraySongs, chosen: 0});

					//add first selection on default
					$scope.playlist.songs.push(arraySongs[0]);
					$scope.playlist.imgURL = $scope.imgURL + arraySongs[0].imgURL;
					$scope.searchTerm = '';

					ListUp.scrollToAddedSong($element); //TODO: do it better way
				}
			});
		};

		$scope.selectSong = function (songIndex, selectedSong, songs) {
			$scope.playlist.imgURL = '';
			$scope.selectedIndex = selectedSong;
			songs.chosen = selectedSong;
			$scope.playlist.songs[songIndex] = $scope.songsOptions[songIndex].songs[selectedSong];
			$scope.playlist.imgURL = $scope.playlist.imgURL + $scope.songsOptions[songIndex].songs[selectedSong].imgURL;
		};

		$scope.confirmSongDelete = function (songs) {
			var index = ListUp.inArray(songs, $scope.songsOptions);
			$scope.openedPlayingSongPop = false;
			$scope.openedSongPop = false;
			$scope.deletingIndex = index;
			if (($routeParams.playlistId === ListUp.playingPlaylist) && ((ListUp.playing !== false) || (ListUp.pausing !== false))) {
				$scope.openedPlayingSongPop = true;
			} else {
				$scope.deleteSong();
			}
		};

		$scope.deleteSong = function () {
			$scope.songsOptions.splice($scope.deletingIndex, 1);
			$scope.playlist.songs.splice($scope.deletingIndex, 1);
		};

		$scope.confirmPlayingPlaylist = function () {
			$scope.openedPlayingPlaylistPop = false;
			if ($routeParams.playlistId === undefined || $routeParams.playlistId !== ListUp.playingPlaylist) {
				$scope.savePlaylist();
			} else {
				$scope.openedPlayingPlaylistPop = true;
			}
		};

		//TODO: unused due to ordering
		$scope.reversePlaylist = function () {
			var i,
				newPositions = [];

			for (i = $scope.playlist.songs.length - 1; i >= 0; i--) {
				newPositions.push(i);
			}

			$scope.playlist.songs = ListUp.applyNewOrder.call(this, $scope.playlist.songs, newPositions);
			$scope.songsOptions = ListUp.applyNewOrder.call(this, $scope.songsOptions, newPositions);

			$scope.$apply();
		};
		//TODO: unused due to ordering
		$scope.shufflePlaylist = function () {
			var positions = [],
				positionIndex,
				newPositions = [];

			//create positions to choose from
			angular.forEach($scope.playlist.songs, function (song, i) {
				positions.push(i);
			});

			//randomly pick position
			while (positions.length > 0) {
				positionIndex = Math.floor(Math.random() * positions.length);
				newPositions.push(positions.splice(positionIndex, 1)[0]); //remove picked position element
			}

			$scope.playlist.songs = ListUp.applyNewOrder.call(this, $scope.playlist.songs, newPositions);
			$scope.songsOptions = ListUp.applyNewOrder.call(this, $scope.songsOptions, newPositions);

			$scope.$apply();
		};


		$scope.savePlaylist = function () {
			if (!$routeParams.playlistId) {
				if ($scope.playlist.imgURL === undefined) {
					$scope.playlist.imgURL = './css/logoPlaylistGreen.png';
				}
				Playlist.save($scope.playlist, function () {
					//reset
					ListUp.globalCreatePlaylist = {
						songsOptions: [],
						playlist: {
							songs: [],
							vote: 0,
							suggest: 0,
							visibility: 'all'
						}
					};
					$location.path('/playlists');
				});
			} else {
				$scope.playlist.$update({playlistId: $routeParams.playlistId}, function () {
					$location.path('/playlists/show/' + $routeParams.playlistId);
				});
			}
		};

		$scope.view = function (playlistView) {
			$scope.playlistView = playlistView;
		};

		$scope.acceptSuggestion = function (songs) {
			var index = ListUp.inArray(songs, $scope.songsOptions);

			$scope.playlist.songs[index].isAccepted = 1;

			//put accpeted at the end
			$scope.playlist.songs.push($scope.playlist.songs.splice(index, 1)[0]);
			$scope.songsOptions.push($scope.songsOptions.splice(index, 1)[0]);
		};

		$scope.confirmTitle = function () {
			if (!$scope.playlist.title) {
				$scope.openedTitlePop = true;
			} else {
				$scope.openedTitlePop = false;
			}
		};
	});
}(angular));
