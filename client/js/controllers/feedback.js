(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('FeedbackCtrl', function ($scope, $http, $location, ListUp) {
		$scope.feedback = {};
		$scope.openedFeedbackSentPop = false;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.returnPath !== undefined) {
				ListUp.skipReload = true;
			}
		};

		$scope.sendFeedback = function () {
			if ($scope.feedback.opinion !== undefined || $scope.feedback.comment) {
				$scope.openedFeedbackSentPop = true;
				$scope.feedback.url = ListUp.returnUrl;
				ListUp.returnUrl = undefined;

				$http.post('../api/1.0/feedback', angular.toJson($scope.feedback)).success(function () {
					$scope.feedback = {};
					$scope.showInput = false;
				});
			}
		};

		$scope.goToPrevious = function () {
			$scope.openedFeedbackSentPop = false;
			if (ListUp.returnPath !== undefined) {
				$location.path(ListUp.returnPath);
				ListUp.returnPath = undefined;
			} else {
				$location.path('/');
			}
		};
	});
}(angular));
