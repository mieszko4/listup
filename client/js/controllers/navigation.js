(function (angular) {
	"use strict";
	/*global angular, console*/

	angular.module('listup').controller('NavigationCtrl', function ($scope, ListUp, UserPreferencesService, $location) {
		$scope.ListUp = ListUp;

		$scope.changeVolume = function () {
			if (ListUp.songObject !== undefined) {
				ListUp.songObject.setVolume(ListUp.volume);
			}
			UserPreferencesService.set('global-volume', ListUp.volume);
		};

		$scope.jumpTo = function () {
			var songPosition;

			if (ListUp.songObject !== undefined) {
				songPosition = ListUp.songProgress * ListUp.songObject.durationEstimate / 1000;
				ListUp.songObject.setPosition(songPosition);
			}
		};

		$scope.jumpToSong = function () {
			var newPath = '/playlists/show/' + ListUp.playingPlaylist;

			if ($location.path() === newPath) {
				$scope.$parent.jumpToSong.call($scope.$parent);
			} else {
				$location.path(newPath);
			}
		};

		$scope.stopSong = function () {
			ListUp.playing = false;
			ListUp.pausing = false;
			ListUp.playingPlaylist = undefined;
			ListUp.songObject.stop();
			ListUp.songObject = undefined;

			ListUp.repaintScreen($scope);
		};

		$scope.toggleSong = function () {
			ListUp.playing = !ListUp.playing;
			ListUp.pausing = true;
			ListUp.songObject.togglePause();
		};
	});
}(angular));