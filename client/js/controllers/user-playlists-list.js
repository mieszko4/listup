(function (angular) {
	"use strict";
	/*global angular, window*/

	angular.module('listup').controller('UserPlaylistsCtrl', function ($scope, $location, $routeParams, LogService, Playlist, ListUp, User) {
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			$scope.showPrivate = false;
			$scope.showPublic = false;

			$scope.playlists = Playlist.query({creator: $routeParams.userId}, function () {
				angular.forEach($scope.playlists, function (p) {
					if (p.visibility === 'all') {
						$scope.showPublic = true;
					} else {
						$scope.showPrivate = true;
					}
				});
			});

			$scope.user = User.get({userId: $routeParams.userId});

			if (ListUp.userId === $routeParams.userId) {
				$scope.isOwnPage = true;
			} else {
				$scope.isOwnPage = false;
			}
		};

		$scope.logout = function () {
			LogService.logout();
			$location.path('/');
		};

		$scope.confirmPlaylistDelete = function (playlist) {
			$scope.deletingPlaylist = playlist;
			$scope.openedDeletePlaylistPop = false;
			if ((ListUp.playingPlaylist === playlist.id) && ((ListUp.playing !== false) || (ListUp.pausing !== false))) {
				$scope.openedPlayingPlaylistPop = true;
			} else {
				$scope.openedDeletePlaylistPop = true;
			}
		};

		$scope.deletePlaylist = function () {
			Playlist.remove({playlistId: $scope.deletingPlaylist.id});

			$scope.playlists.splice(ListUp.inArray($scope.deletingPlaylist, $scope.playlists), 1);
			$scope.deletingPlaylist = undefined;
			$scope.openedDeletePlaylistPop = false;
		};
	});
}(angular));