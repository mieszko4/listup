(function (angular) {
	"use strict";
	/*global angular, console*/

	//TODO: SongService is not used but is needed for initialization
	angular.module('listup').controller('PlaylistDetailCtrl', function ($scope, $routeParams, $rootScope, $location, $http, Playlist, SongService, ListUp, UserPreferencesService, $window, $element) {
		var SCid;

		$scope.port = $location.port();
		$scope.ListUp = ListUp;

		$scope.onActivate = function () {
			ListUp.repaintScreen($scope);
			if (ListUp.skipReload) {
				ListUp.skipReload = undefined;
				return;
			}

			$scope.opened = false;

			if ($routeParams.playlistId === ListUp.playingPlaylist) {
				$scope.selectedIndex = ListUp.playingIndex;
			}

			$scope.playlist = Playlist.get({playlistId: $routeParams.playlistId}, function () {
				//TODO: automatically scroll when comming from other page
			});
			$scope.currentPlaylistId = $routeParams.playlistId;
		};

		$scope.repeat = function () {
			ListUp.repeatPlaylist = !ListUp.repeatPlaylist;
			UserPreferencesService.set('repeat-playlist', ListUp.repeatPlaylist);
		};

		$scope.jumpToSong = function () {
			ListUp.scrollToSong($element, $scope.selectedIndex); //TODO: do it better way
		};

		$scope.addSong = function (song) {
			var songOption = {};

			songOption = {
				"artist": song.artist,
				"title": song.title,
				"search_term": song.search_term,
				"imgURL": song.imgURL,
				"SCid": song.SCid,
				"isAccepted": 1,
				"$index": ListUp.globalCreatePlaylist.playlist.songs.length
			};

			ListUp.globalCreatePlaylist.songsOptions.push({songs: [songOption], chosen: 0});
			ListUp.globalCreatePlaylist.playlist.songs.push(songOption);
		};

		$scope.toggleSong = function (index, internal) {
			var currentSongObject = ListUp.songObject,
				getNextSong = function () {
					index = index + 1;
					if (index < ListUp.currentPlaylistSongs.length) {
						$scope.toggleSong(index, true);
					} else {
						console.log('Last song was played');
						if (ListUp.repeatPlaylist) {
							index = 0;
							$scope.toggleSong(index, true);
						} else {
							ListUp.playing = false;
							ListUp.pausing = false;
							ListUp.playingPlaylist = undefined;
							ListUp.songObject.stop();
							ListUp.songObject = undefined;

							$rootScope.$apply();
							ListUp.repaintScreen($scope);
						}
					}
				};

			// ako postoji pjesma koja se svirala prije i nije izbrisana onda je zaustavi prije nego kreneš svirati sljedeću
			if (currentSongObject !== undefined) {
				if (!internal) {
					if (ListUp.playingIndex === index && ListUp.playingPlaylist === $scope.playlist.id) {
						currentSongObject.togglePause();
						ListUp.playing = !ListUp.playing;
						ListUp.pausing = true;
						if (ListUp.playing) {
							ListUp.pausing = false;
						}
					} else {
						currentSongObject.stop();
						$scope.selectedIndex = undefined;
						ListUp.playingIndex = undefined;
						ListUp.playingPlaylist = undefined;
						ListUp.playing = false;
						ListUp.songObject = undefined; //destroy

						console.log('New playlist started');
						$scope.toggleSong(index, internal);
					}
					return;
				} else {
					//ignore
				}
			} else {
				console.log('Playlist started');
				ListUp.playingPlaylist = $scope.playlist.id;
				ListUp.currentPlaylistSongs = $scope.playlist.songs;
			}

			//skip not accepted
			if (ListUp.currentPlaylistSongs[index].isAccepted == "0") { //TODO: change isAccepted type in API
				$scope.toggleSong(index + 1, true);
				return;
			}

			SCid = ListUp.currentPlaylistSongs[index].SCid;
			$window.SC.stream(
				"/tracks/" + SCid,
				{
					multiShotEvents: true,
					preferFlash: false,
					useHTML5Audio: true,
					volume: ListUp.volume,
					onfinish: function () {
						getNextSong.call(this);
					},
					whileplaying: function () {
						var progress = 1000 * this.position / this.durationEstimate,
							minOffset = 1; //in %

						if (Math.abs(progress - ListUp.songProgress) > minOffset) {
							progress = Math.floor(progress);
							ListUp.songProgress = progress;

							$rootScope.$apply();
						}
					},
					onload: function (success) {
						if (!success) {
							console.warn('Error with playing this song', 'skipping');
							getNextSong.call(this);
						}
					}
				},
				function (song) {
					ListUp.songProgress = 0;
					song.play();

					$scope.selectedIndex = index;
					$rootScope.$apply($scope.selectedIndex); // update class in playlist-details view

					//TODO: Is this the right way?
					$rootScope.$digest(ListUp.playingTitle = ListUp.currentPlaylistSongs[$scope.selectedIndex].title);
					$rootScope.$digest(ListUp.playingIndex = $scope.selectedIndex);
					$rootScope.$digest(ListUp.playing = true);
					$rootScope.$digest(ListUp.songObject = song);

					ListUp.repaintScreen($scope);
				}
			);
		};

		$scope.confirmPlaylistDelete = function () {
			$scope.openedDeletePlaylistPop = false;
			$scope.openedDeletePlayingPlaylistPop = false;
			if (($routeParams.playlistId === ListUp.playingPlaylist) && ((ListUp.playing !== false) || (ListUp.pausing !== false))) {
				$scope.openedDeletePlayingPlaylistPop = true;
			} else {
				$scope.openedDeletePlaylistPop = true;
			}
		};

		$scope.deletePlaylist = function () {
			$scope.playlist.$remove({playlistId: $routeParams.playlistId});
			$location.path('/playlists');
			$scope.openedDeletePlaylistPop = false;
		};

		$scope.suggest = function () {
			$location.path('/playlists/suggest/' + $routeParams.playlistId);
			if (!ListUp.loggedIn) {
				$scope.$emit('event:auth-loginRequired');
			}
		};

		$scope.forkPlaylist = function () {
			$http.post('../api/1.0/playlists/' + $routeParams.playlistId + '/fork', $scope.data).success(function () {
				$location.path('/playlists');
			});
		};

		$scope.showSharePop = function () {
			$scope.openedSharePop = true;

			if ($window.FB) {
				$window.FB.XFBML.parse();
			}

			if ($window.gapi) {
				var btns = angular.element('.gplusone'),
					href = btns.attr('href');

				$window.gapi.plusone.render(btns[0], {
					'href': href,
					'size': 'medium'
				});
			}
		};

	});
}(angular));
