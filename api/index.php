<?php
date_default_timezone_set('Europe/Zagreb');
session_start();

class ResourceNotFoundException extends Exception {}
class DataNotValidException extends Exception {}
class DataError extends Exception {}


require '../libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

#get config
$LUPConfig = json_decode(file_get_contents('../configuration.json'));

#get db connection
try {
	$dbh = new PDO('mysql:dbname='.$LUPConfig->db->name.'; host='.$LUPConfig->db->host, $LUPConfig->db->username, $LUPConfig->db->password);
	$dbh->exec("set names utf8");
} catch (PDOException $e) {
	echo 'Connection failed: ' . $e->getMessage();
}
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_NAMED);

$app = new \Slim\Slim();

#if user is not login send 401 and stop execution
$app->hook('slim.before.dispatch', function () use ($app, $LUPConfig) {
	$path = $app->request()->getResourceUri();
	$method = $app->request()->getMethod();

	if( !(
			($method === 'GET' && $path === $LUPConfig->api->prefix.'playlists') 
		|| 	($method === 'GET' && preg_match("/".preg_quote($LUPConfig->api->prefix, '/')."playlists\\/[\d]+$/", $path)) 
		|| 	($method === 'POST' && $path === $LUPConfig->api->prefix.'users') 
		|| 	(($method === 'POST' || $method === 'GET') && $path === $LUPConfig->api->prefix.'login') 
		|| 	($method === 'POST' && $path === $LUPConfig->api->prefix.'feedback') 
		|| 	($method === 'POST' && $path === $LUPConfig->api->prefix.'logout') 
		|| 	(preg_match("/".preg_quote($LUPConfig->api->prefix, '/')."notifications\\/?[\d]*$/", $path))
		) ) {
			if(!isset($_SESSION['user_id'])) {
				$app->response()->status(401);
				$app->stop(); 
			}
		}
});

#load routes
include 'routes/users.php';
include 'routes/playlists.php';
include 'routes/likes.php';

include 'routes/feedbacks.php';
include 'routes/forks.php';
include 'routes/suggestions.php';
include 'routes/authentications.php';
include 'routes/notifications.php';

$app->run();
?>
