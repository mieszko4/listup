<?php
require_once('helpers/Helper.php');
require_once('resources/Like.php');

$app->post($LUPConfig->api->prefix.'likes/:id', function($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Like($app, $dbh), 'post', $id);
});

?>