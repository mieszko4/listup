<?php 
require_once('helpers/Helper.php');
require_once('resources/Playlist.php');

// 1.) GET -> dohvati SVE playliste
// 		a) /api/1.0/playlists?creator=1 
//      		- dohvati sve playliste usera s id=1
//		b) /api/1.0/playlists
//				- dohvati sve playliste svih usera
$app->get($LUPConfig->api->prefix.'playlists', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Playlist($app, $dbh), 'getAll', NULL);
});

// 2.) GET/:id -> dohvati pojedinu playlistu 
$app->get($LUPConfig->api->prefix.'playlists/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Playlist($app, $dbh), 'get', $id);
}); 

// 3.) POST -> dodaj 1 novu playlistu
$app->post($LUPConfig->api->prefix.'playlists', function() use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Playlist($app, $dbh), 'post', NULL);
});

// 4.) PUT -> update 1 playlist of whom you are owner
$app->put($LUPConfig->api->prefix.'playlists/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Playlist($app, $dbh), 'put', $id);
});

// 6.) DELETE -> izbrisi playlistu i sve pjesme iz te playliste *****
$app->delete($LUPConfig->api->prefix.'playlists/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Playlist($app, $dbh), 'delete', $id);
});

?>
