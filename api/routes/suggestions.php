<?php
require_once('helpers/Helper.php');
require_once('actions/Suggestion.php');

$app->post($LUPConfig->api->prefix.'playlists/:id/send_suggestion', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Suggestion($app, $dbh), 'send_suggestion', $id);
});

$app->post($LUPConfig->api->prefix.'playlists/:id/accept_suggestion', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Suggestion($app, $dbh), 'accept_suggestion', $id);
});

?>