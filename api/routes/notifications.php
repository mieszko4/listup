<?php 
require_once('helpers/Helper.php');
require_once('resources/Notification.php');

$app->get($LUPConfig->api->prefix.'notifications', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Notification($app, $dbh), 'getAll', NULL);
});

$app->get($LUPConfig->api->prefix.'notifications/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Notification($app, $dbh), 'get', $id);
}); 

$app->post($LUPConfig->api->prefix.'notifications', function() use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Notification($app, $dbh), 'post', NULL);
});

$app->put($LUPConfig->api->prefix.'notifications/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Notification($app, $dbh), 'put', $id);
});

$app->delete($LUPConfig->api->prefix.'notifications/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Notification($app, $dbh), 'delete', $id);
});

?>
