<?php 
require_once('helpers/Helper.php');
require_once('resources/User.php');

//   1. GET -> retrieve ALL users
$app->get($LUPConfig->api->prefix.'users', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new User($app, $dbh), 'getAll', NULL);
}); 

//   2. GET:id -> retrieve user with id=:id
$app->get($LUPConfig->api->prefix.'users/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new User($app, $dbh), 'get', $id);
});

// 3. POST -> user REGISTRATION
$app->post($LUPConfig->api->prefix.'users', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new User($app, $dbh), 'post', NULL);
});

//   4. PUT -> update user
$app->put($LUPConfig->api->prefix.'users/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new User($app, $dbh), 'put', $id);
});
//   PATCH -> partial update
/* RESPONSE FORMAT (as array->it is IMPORTANT that it is an array):
   [
		{
			"path": "username",
			"value": "eni" 
		}
	]
   
   or
   
   [
		{ 
			"path": "username",
			"value": "eni" 
		},
		{
			"path":"password",
			"value":"eni2"
		}
	]

*/
#TODO: revise it. 1: wrong input format, 2: should that be PATCH?
/*
$app->post($LUPConfig->api->prefix.'users/:id', function ($id) use ($app, $dbh) {
	try {		
		// user can edit only his profile 
		if( $id != $_SESSION['user_id']) {
			throw new DataError('', 403);
		} else {			
			$request = json_decode($app->request()->getBody());
			foreach ($request as $req) {
				//check if there is user with same username
				if($req->path === 'username') {
					$sth = $dbh->prepare('SELECT * FROM users WHERE username=? AND NOT id=?');
					$sth->bindParam(1, $req->value);
					$sth->bindParam(2, $id);
					$sth->execute();
					if($sth->fetch()) {
						throw new DataError('Someone already has that username', 409);
					}
				} 
				// check if email is valid
				if($req->path === 'email') {
					if(!filter_var($req->value, FILTER_VALIDATE_EMAIL)) {
						throw new DataError('Email is not valid', 400);
					}
				} 
		
				$sth = $dbh->prepare('UPDATE users SET ' . $req->path . '=? WHERE id=?');
				if($req->path === 'password') {
					$password = hash('sha256', $req->value);
					$sth->bindParam(1, $password);
				} else {
					$sth->bindParam(1, $req->value);
				}
				$sth->bindParam(2, $id);
				$sth->execute();
			}
			$sth = $dbh->prepare('SELECT * FROM users WHERE id=?');
			$sth->bindParam(1, $id);
			$sth->execute();
			$app->response()->header('Content-Type', 'application/json');
			echo json_encode($sth->fetch());			
		}
	} catch (DataError $e) {
		$app->response()->status($e->getCode());
		$app->response()->header('X-Status-Reason', $e->getMessage());
	} catch (PDOException $e) {
		$app->response()->status(500);
		$app->response()->header('X-Status-Reason', $e->getMessage());
	}
});
*/
	
//   6. DELETE -> delete user
$app->delete($LUPConfig->api->prefix.'users/:id', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new User($app, $dbh), 'delete', $id);
});
?>