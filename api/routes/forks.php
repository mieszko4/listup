<?php
require_once('helpers/Helper.php');
require_once('actions/Fork.php');

$app->post($LUPConfig->api->prefix.'playlists/:id/fork', function ($id) use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Fork($app, $dbh), 'post', $id);
});

?>