<?php
require_once('helpers/Helper.php');
require_once('actions/Feedback.php');

$app->post($LUPConfig->api->prefix.'feedback', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Feedback($app, $dbh), 'post', NULL);
});

?>