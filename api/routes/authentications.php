<?php
require_once('helpers/Helper.php');
require_once('actions/Authentication.php');

$app->post($LUPConfig->api->prefix.'login', function () use ($app, $dbh, $LUPConfig) {
	$helper = new Helper($app);
	$helper->tryAction(new Authentication($app, $dbh), 'post', $LUPConfig);
});

$app->get($LUPConfig->api->prefix.'login', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Authentication($app, $dbh), 'get', NULL);
});

$app->post($LUPConfig->api->prefix.'logout', function () use ($app, $dbh) {
	$helper = new Helper($app);
	$helper->tryAction(new Authentication($app, $dbh), 'logout', NULL);
});

?>