<?php
	$playlist_url = "http://$_SERVER[HTTP_HOST]" . "/api/1.0/playlists/" . $playlist['id'];
	$playlist_url_hashed = "http://$_SERVER[HTTP_HOST]" . "/#!/playlists/show/" . $playlist['id'];	
?>

<html>
	<head>
		<title><?php echo $playlist['title']; ?> by <?php echo $playlist['creatorUsername']; ?>- Free Music Streaming, Online Music - ListUp</title>
		<meta name="title" content="<?php echo $playlist['title']; ?> by <?php echo $playlist['creatorUsername']; ?>" />
		
		<meta property="fb:app_id"          content="175972395921488" /> 
		<meta property="og:type"            content="music.playlist" /> 
		<meta property="og:url"             content="<?php echo $playlist_url; ?>" /> 
		<meta property="og:title"           content="<?php echo $playlist['title']; ?>" /> 
		<meta property="og:image"           content="<?php echo $playlist['imgURL']; ?>" /> 
    
		<link rel="image_src" href="<?php echo $playlist['imgURL']; ?>" />
		<link rel="canonical" href="<?php echo $playlist_url; ?>" />

		<meta name="description" property="og:description" content="Play <?php echo $playlist['title']; ?> for free on ListUp." />
		<meta name="keywords" content="listup playlist, <?php echo $playlist['title']; ?>" />

		<script type="text/javascript">
			(function() {
				window.location = "<?php echo $playlist_url_hashed; ?>"; 
			})();
		</script>
		
	</head>
	
	<body>
	</body>
</html>
