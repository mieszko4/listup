<?php

require_once('helpers/Helper.php');

// Base resource
abstract class Base {

	protected $app, $dbh;
	
	function __construct($app, $dbh) {
		$this->app = $app;
		$this->dbh = $dbh;
	}
	
	function createAndExecInsertSth($tablename, $params) {
		$names = array_keys($params);
		$placeholders = array();
		foreach ($params as $param => $value) {
			$placeholders[":$param"] = $value;
		}

		$query = 'INSERT INTO `' . $tablename . '`(' . implode(',', array_keys($params)) .') VALUES (' . implode(',', array_keys($placeholders)) . ')';

		return $this->createNamedSthAndExec($query, $placeholders);
	}

	function createNamedSthAndExec($query, $params) {
		$sth = $this->dbh->prepare($query);
		foreach ($params as $param => $value) {
			if (is_array($value)) {
				$sth->bindValue($param, $value[0], $value[1]);
			} else {
				$sth->bindValue($param, $value);
			}
		}
		$sth->execute();
		return $sth;
	}

	function createSthAndExec($query, $params) {
		$sth = $this->createSth($query, $params);
		$sth->execute();
		return $sth;
	}
	
	function createSth($query, $params) {
		return Helper::createSth($this->dbh, $query, $params);
	}
	
	function getUserId() {
		if (isset($_SESSION['user_id'])) {
			return $_SESSION['user_id'];
		} else {
			return null;
		}
	}
	
	function setJsonHeader() {
		$this->app->response()->header('Content-Type', 'application/json');
	}
	
	function setStatus201() {
		$this->app->response()->status(201);
	}
}

?>