<?php

require_once('resources/Base.php');

class Notification extends Base {
	// User must be logged in. Check index.php

	function getAll() {
		$userId = $this->getUserId();
		
		$sth = $this->createSthAndExec('
			SELECT n.id, n.user_id, n.referenced_playlist_id, n.is_new, n.type, n.datetime_created, n.datetime_modified 
			FROM notifications AS n
			INNER JOIN playlists AS p ON p.id = n.referenced_playlist_id 
			WHERE p.id_creator = ? AND n.is_new = 1'
		, array($userId));
		
		$this->setJsonHeader();
		echo json_encode($sth->fetchAll());
	}

	function get($id) {
		$userId = $this->getUserId();
	
		$sth = $this->createSthAndExec('
			SELECT n.id, n.user_id, n.referenced_playlist_id, n.is_new, n.type, n.datetime_created, n.datetime_modified 
			FROM notifications AS n
			INNER JOIN playlists AS p ON p.id = n.referenced_playlist_id 
			WHERE n.id = ? 
			AND p.id_creator = ?'
		, array($id, $userId));
		
		if($notification = $sth->fetch()) {
			$this->setJsonHeader();
			echo json_encode($notification);
		} else {
			throw new DataError('Notification not found',404);
		}
	}

	function post() {
		$request = json_decode($this->app->request()->getBody());
		$userId = $this->getUserId();
		
		$sth = $this->createSthAndExec('
			INSERT INTO notifications(user_id, referenced_playlist_id, is_new, type, datetime_created, datetime_modified) 
			VALUES (?, ?, ?, ?, NOW(), NOW())',
			array($userId, $request->referenced_playlist_id, $request->is_new, $request->type));		
			
		$this->setStatus201();
		$this->setJsonHeader();
		echo json_encode(array("id"=>$this->dbh->lastInsertId()));
	}

	function put($id) {
		$userId = $this->getUserId();
		$request = json_decode($this->app->request()->getBody());

		$sth = $this->createSthAndExec('
			UPDATE notifications 
			INNER JOIN playlists ON playlists.id = notifications.referenced_playlist_id
			SET notifications.is_new = ?, notifications.datetime_modified = NOW() 
			WHERE notifications.id = ? 
			AND playlists.id_creator= ?',
				array($request->is_new, $id, $userId));
			
		$sth = $this->createSthAndExec('
			SELECT n.id, n.user_id, n.referenced_playlist_id, n.is_new, n.type, n.datetime_created, n.datetime_modified 
			FROM notifications AS n
			INNER JOIN playlists AS p ON p.id = n.referenced_playlist_id 
			WHERE n.id = ?'
			, array($id));
			
		$this->setJsonHeader();
		echo json_encode($sth->fetch());
	}

	function delete($id) {
		$userId = $this->getUserId();
		
		$sth = $this->createSthAndExec('
			DELETE notifications, playlists FROM notifications
			INNER JOIN playlists ON notifications.referenced_playlist_id = playlists.id
			WHERE notifications.id = ? AND playlists.id_creator = ?'
		, array($id, $userId));

		$this->app->response()->status(204);
	}

}
?>