<?php

require_once('resources/Base.php');

class Playlist extends Base {
	
	function getAll() {
		$limit = isset($_GET['limit']) ? intval($_GET['limit']) : 1000; //hardcoded limit
		$skip = isset($_GET['skip']) ? intval($_GET['skip']) : 0;
		$orderBy = (isset($_GET['orderBy']) && in_array($_GET['orderBy'], array('time', 'id'))) ? $_GET['orderBy'] : 'id';
		$orderType = (!isset($_GET['desc']) || $_GET['desc'] === 'false') ? 'ASC' : 'DESC';

		$creator = isset($_GET['creator']) ? $_GET['creator'] : null;
		$visibility = (isset($_GET['visibility']) && in_array($_GET['visibility'], array('all', 'private'))) ? $_GET['visibility'] : null;

		$params = array(
			':skip' => array($skip, PDO::PARAM_INT),
			':limit' => array($limit, PDO::PARAM_INT)
		);

		if (!isset($visibility)) { //both
			$conditions = ' WHERE playlists.visibility="all" OR playlists.visibility="private" AND playlists.id_creator=:owner';
			$params[':owner'] = $this->getUserId();
		} elseif ($visibility === 'all') {
			$conditions = ' WHERE playlists.visibility="all"';
		} elseif ($visibility === 'private') {
			$conditions = ' WHERE playlists.visibility="private" AND playlists.id_creator=:owner';
			$params[':owner'] = $this->getUserId();
		}
		
		$query = '
			SELECT
				playlists.id,
				title,
				username AS creatorUsername,
				id_creator AS creatorId,
				imgURL,
				time,
				vote,
				suggest,
				referer,
				visibility
			FROM playlists JOIN users
				ON playlists.id_creator=users.id' .
				(isset($creator) ? ' AND users.id=:creator' : '') .
			$conditions . '
			ORDER BY playlists.`' . $orderBy . '` ' . $orderType . '
			LIMIT :skip, :limit
		';

		if (isset($creator)) {
			$params[':creator'] = $creator;
		}
		
		$sth = $this->createNamedSthAndExec($query, $params);
		$allPl = $sth->fetchAll();

		$this->app->response()->header('Content-Type', 'application/json');
		echo json_encode($allPl);  
	}
	
	function get($id) {
		$sth = $this->createSthAndExec('SELECT * FROM playlists WHERE id=?', array($id));
		
		$playlist = $sth->fetch();
		if($playlist) {
			//check visibility
			if (!($playlist['visibility'] === 'all' || ($playlist['visibility'] === 'private' && $playlist['id_creator'] === $this->getUserId()))) {
				throw new DataError('You are not allowed to see this playlist', 403);
			}

			// is owner ??
			if ($this->getUserId() !== null) {
				$sth = $this->createSthAndExec('SELECT id FROM playlists WHERE id=? AND id_creator=?', array($id, $_SESSION['user_id']));
				$isOwner = ($sth->fetch())?1:0;
						
				//likes
				$sth = $this->createSthAndExec('SELECT id_playlist FROM likes, playlists WHERE likes.id_playlist=playlists.id AND id_user=? AND id_playlist=?', array($_SESSION['user_id'], $id));
				$liked = ($sth->fetch())?1:0;
			} else {
				$liked = 0;
				$isOwner = 0;
			}
			
			$sth = $this->createSthAndExec('SELECT playlists.id, title, username AS creatorUsername, id_creator AS creatorId, imgURL, time, vote, suggest, referer, COUNT(id_playlist) AS numLikes,' . $liked . ' AS liked, visibility
								  FROM playlists LEFT OUTER JOIN likes ON likes.id_playlist=id
								  JOIN users ON playlists.id_creator=users.id
								  WHERE playlists.id=? 
								  GROUP BY id, id_playlist', array($id));
			
			$playlist = $sth->fetch();
			
			// if owner then give back songs with isAccepted true and false
			// not owner - give back only songs with isAccepted true
			$query = 'SELECT songs.id, artist, title, search_term, imgURL, SCid, isAccepted, suggestor, users.username AS suggestor_name 
						FROM songs 
						LEFT JOIN users ON songs.suggestor=users.id 
						WHERE songs.id_playlist=?
						ORDER BY order_position, songs.id';
			$query = $isOwner?$query:($query.' AND isAccepted=true');
			$sth = $this->createSthAndExec($query, array($id));
			
			$songs = $sth->fetchAll();
			
			$isXHR = $this->app->request()->isXhr();
			if ($isXHR === true) {			
				$this->app->response()->header('Content-Type', 'application/json');			
				echo json_encode(array('id'=>$playlist['id'], 'title'=>$playlist['title'], 'creatorUsername'=>$playlist['creatorUsername'], 'creatorId'=>$playlist['creatorId'], 'imgURL'=>$playlist['imgURL'], 'time'=>$playlist['time'], 'vote'=>$playlist['vote'], 'suggest'=>$playlist['suggest'], 'referer'=>$playlist['referer'], 'numLikes'=>$playlist['numLikes'], 'liked'=>$playlist['liked'], 'songs'=>$songs, 'visibility'=>$playlist['visibility']));
			} else {
				$this->app->response()->header('Content-Type', 'text/html');
				$this->app->render('playlist.php', array('playlist' => $playlist));
			}
		} else {
			throw new DataError('Playlist not found', 404);
		}
	}

	function post() {
		$request = json_decode($this->app->request()->getBody());
		$time = date('Y-m-d H:i:s');
		$sth = $this->createSthAndExec('INSERT INTO playlists(title, id_creator, imgURL, time, vote, suggest, visibility) VALUES (?, ?, ?, ?, ?, ?, ?)',
						array($request->title, $_SESSION['user_id'], $request->imgURL, $time, $request->vote, $request->suggest, $request->visibility));
		
		$lastPlaylistId = $this->dbh->lastInsertId();
		
		foreach($request->songs as $index=>$song) { //song array comes in right order from client
			$sth = $this->createSthAndExec('INSERT INTO songs(artist, title, id_playlist, search_term, imgURL, SCid, order_position) VALUES (?, ?, ?, ?, ?, ?, ?)',
				array($song->artist, $song->title, $lastPlaylistId, $song->search_term, $song->imgURL, $song->SCid, $index));
		}
		
		// vrati json nove playliste
		$sth = $this->createSthAndExec('SELECT playlists.id, title, username, imgURL, time, vote, suggest, visibility FROM playlists, users WHERE playlists.id_creator=users.id AND playlists.id=?',
						array($lastPlaylistId));

		$playlist= $sth->fetch(PDO::FETCH_OBJ);
		
		$sth = $this->createSthAndExec('SELECT id, artist, title, search_term, imgURL, SCid FROM songs WHERE id_playlist=?',
						array($lastPlaylistId));

		$this->app->response()->header('Content-Type', 'application/json');
		echo json_encode(array('id'=>$playlist->id, 'title'=>$playlist->title, 'creatorUsername'=>$playlist->username, 'imgURL'=>$playlist->imgURL, 'time'=>$playlist->time, 'vote'=>$playlist->vote, 'suggest'=>$playlist->suggest, 'visibility'=>$playlist->visibility, 'songs'=>$sth->fetchAll()));
	}

	function put($id) {
		// you have to be playlist owner to edit it
		$sth = $this->createSthAndExec('SELECT * FROM playlists WHERE id=? AND id_creator=?',
						array($id, $_SESSION['user_id']));

		if(!$sth->fetch()) {
			throw new DataError('You have to be playlist owner to edit it', 403);
		} else {	
			$request = json_decode($this->app->request()->getBody());
			
			$time = date('Y-m-d H:i:s');
			$sth = $this->createSthAndExec('UPDATE playlists SET title=?,imgURL=?, time=?, vote=?, suggest=?, visibility=? WHERE playlists.id=? AND id_creator=?',
							array($request->title, $request->imgURL, $time, $request->vote, $request->suggest, $request->visibility, $id, $_SESSION['user_id']));			
			
			// DELETE SONGS 
			$keepSongsId = Array();
			foreach($request->songs as $song) {
				if(isset($song->id)) {
					array_push($keepSongsId, $song->id);
				}
			}	
			
			if(count($keepSongsId)>0) {
				$numParam = '(';
				for($i=0; $i<count($keepSongsId)-1; $i++) {
					$numParam .= '?,';
				};
				$numParam .= '?)';
				
				$paramss = array($id);
				for ($i=0; $i<count($keepSongsId); $i++) {
					array_push($paramss, $keepSongsId[$i]);
				} 
				
				$sth = $this->createSthAndExec('SELECT id FROM songs WHERE id_playlist=? AND id NOT IN ' . $numParam, 
								$paramss);
				
				if($deleteSongsId = $sth->fetchAll(PDO::FETCH_COLUMN, 0)) {
					foreach ($deleteSongsId as $deleteSongId) {
						$sth = $this->createSthAndExec('DELETE FROM songs WHERE id_playlist=? AND id=?',
										array($id, $deleteSongId));	
					}					
				} 
			} else {
				$sth = $this->createSthAndExec('DELETE FROM songs WHERE id_playlist=?',
								array($id));
			}
			
			// ADD NEW SONG
			foreach($request->songs as $index=>$song) { //song array comes in right order from client
				if(!isset($song->id)) {
					$sth = $this->createSthAndExec('INSERT INTO songs(artist, title, id_playlist, search_term, imgURL, SCid, order_position) VALUES (?, ?, ?, ?, ?, ?, ?)',
									array($song->artist, $song->title, $id, $song->search_term, $song->imgURL, $song->SCid, $index));
				 } else {
					$sth = $this->createSthAndExec('UPDATE songs SET isAccepted=?, order_position=? WHERE id=?',
									array($song->isAccepted, $index, $song->id));
				 }
			}
			
			// vrati json updejtane playliste
			$sth = $this->createSthAndExec('SELECT playlists.id, title, username, imgURL, time, vote, suggest, visibility FROM playlists, users WHERE playlists.id_creator=users.id AND playlists.id=?',
							array($id));
			$playlist = $sth->fetch(PDO::FETCH_OBJ);
			
			$sth = $this->createSthAndExec('SELECT id, artist, title, search_term, imgURL, SCid FROM songs WHERE id_playlist=?',
							array($id));
			$this->app->response()->header('Content-Type', 'application/json');
			echo json_encode(array('id'=>$playlist->id, 'title'=>$playlist->title, 'creatorUsername'=>$playlist->username, 'imgURL'=>$playlist->imgURL, 'time'=>$playlist->time, 'vote'=>$playlist->vote, 'suggest'=>$playlist->suggest, 'visibility'=>$playlist->visibility, 'songs'=>$sth->fetchAll()));
		}
	}

	function delete($id) {
		$sth = $this->createSthAndExec('SELECT * FROM playlists WHERE id=? and id_creator=?', array($id, $_SESSION['user_id']));
		
		if($sth->fetch()) {	
			// delete playlist and all songs
			$sth = $this->createSthAndExec('DELETE FROM playlists WHERE id=?', array($id));

			// delete likes of that playlist
			$sth = $this->createSthAndExec('DELETE FROM likes WHERE id_playlist=?', array($id));
			
			$this->app->response()->status(204);
		} else {
			throw new DataError('', 403);
		}
	}

}


?>