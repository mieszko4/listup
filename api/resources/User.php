<?php

require_once('resources/Base.php');

class User extends Base {
	
	function getAll() {
		$sth = $this->createSthAndExec('SELECT id,username FROM users', NULL);		
		$this->app->response()->header('Content-Type', 'application/json');
		#TODO: avatarUrl
		echo json_encode($sth->fetchAll());
	}

	function get($id) {
		$sth = $this->createSthAndExec('SELECT id,username FROM users WHERE id=?', array($id));
		if($user = $sth->fetch()) {
			$this->app->response()->header('Content-Type', 'application/json');
			#TODO: avatarUrl
			echo json_encode($user);
		} else {
			throw new DataError('',404);
		}
	}

	function post() {
		$request = json_decode($this->app->request()->getBody());
		
		$sth = $this->createSthAndExec('SELECT * FROM users WHERE username=?', array($request->username));

		if($sth->fetch()) {
			throw new DataError('Someone already has that username', 409);
		} else if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
			throw new DataError('Email is not valid', 400);
		} else {
			$type = 'internal';
			$password = hash('sha256', $request->password);
			$sth = $this->createSthAndExec('INSERT INTO users(`type`, username, email, password) VALUES (?, ?, ?, ?)',
							array($type, $request->username, $request->email, $password));
			
			$this->app->response()->status(201);
			$this->app->response()->header('Content-Type', 'application/json');
			echo json_encode(array("id"=>$this->dbh->lastInsertId()));
		}
	}

	function put($id) {
		if($id != $_SESSION['user_id']) {
			throw new DataError('', 403);
		} elseif ($_SESSION['type'] != 'internal') {
			throw new DataError('Only internally registered users can update', 400);
		} else {		
			$request = json_decode($this->app->request()->getBody());
			
			// check if username already exists
			$sth = $this->createSthAndExec('SELECT * FROM users WHERE username=? AND NOT id=?', 
							array($request->username, $id));

			if($sth->fetch()) {
				throw new DataError('Someone already has that username', 409);
			} else if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
				throw new DataError('Email is not valid', 400);
			} else {
				$password = hash('sha256', $request->password);
				$this->createSthAndExec('UPDATE users SET username=?, email=?, password=? WHERE id=?',
						array($request->username, $request->email, $password, $id));
				
				$this->createSthAndExec('SELECT * FROM users WHERE id=?', array($id));
				
				$this->app->response()->header('Content-Type', 'application/json');
				echo json_encode($sth->fetch());
			}				
		}		
	}

	function delete($id) {
		if( $id != $_SESSION['user_id']) {
			throw new DataError('', 403);
		} else {
			$type = $_SESSION['type'];
			
			$this->createSthAndExec('DELETE FROM users WHERE id=? AND `type`=?', array($id, $type));
			session_destroy();
			$this->app->response()->status(204);
			
			if ($type == 'internal') {
				@unlink("../client/avatars/{$_SESSION['user_id']}.jpg");
			}
		}
	}

}
?>