<?php

require_once('resources/Base.php');

class Like extends Base {

	function post($id) {
		//check if playlist is visible
		$sth = $this->createSthAndExec('SELECT username, visibility, id_creator FROM playlists, users WHERE playlists.id=? AND playlists.id_creator=users.id',
				 array($id));

		$playlist = $sth->fetch();
		if (!$playlist) {
			throw new DataError('Playlist not found', 404);
		}

		if (!($playlist['visibility'] === 'all' || ($playlist['visibility'] === 'private' && $playlist['id_creator'] === $this->getUserId()))) {
			throw new DataError('You are not allowed to fork this playlist', 403);
		}

		$sth = $this->createSthAndExec('INSERT INTO likes(id_playlist, id_user) VALUES (?, ?) ', $id, $_SESSION['user_id']);
		
		$this->app->response()->status(204);
	}

}
?>