<?php

class Helper {
	function __construct($app) {
		$this->app = $app;
	}
	
	function tryAction($obj, $funcToExecute, $id) {
		try {
			if (isset($id)) {
				call_user_func(array($obj, $funcToExecute), $id);
			} else {
				call_user_func(array($obj, $funcToExecute));
			}
		} catch (DataError $e) {
			$this->app->response()->status($e->getCode());
			$this->app->response()->header('X-Status-Reason', $e->getMessage());
		} catch (PDOException $e) {
			$this->app->response()->status(500);
			$this->app->response()->header('X-Status-Reason', $e->getMessage());
		} catch (Excepetion $e) {
			$this->app->response()->status(500);
			$this->app->response()->header('X-Status-Reason', $e->getMessage());
		} 
	}
	
	static function createSth($dbh, $query, $params) {
		$sth = $dbh->prepare($query);

		if (isset($params)) {
			for($i = 0, $size = count($params); $i < $size; ++$i) {
				$sth->bindParam($i + 1, $params[$i]);
			}
		}
		
		return $sth;
	}

	static function getAllHeaders() {
		$headers = array();
		if (!function_exists('getallheaders'))  {
			foreach ($_SERVER as $name => $value) {
				if (substr($name, 0, 5) === 'HTTP_') {
					$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
				}
			}
		} else {
			$headers = getallheaders();
		}

		return $headers;
    }
}

?>
