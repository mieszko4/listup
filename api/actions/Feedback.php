<?php
require_once('resources/Base.php');
require_once('helpers/Helper.php');

class Feedback extends Base {
	function post() {
		$request = json_decode($this->app->request()->getBody());
		$params = array(
			'url' => isset($request->url) ? $request->url : null,
			'QUERY_STRING' => $_SERVER['QUERY_STRING'],
			'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
			'REMOTE_PORT' => $_SERVER['REMOTE_PORT']
		);
		$params = array_merge($params, Helper::getAllHeaders());

		$query = $this->createAndExecInsertSth(
			'feedback',
			array(
				'user_id' => isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null,
				'comment' => isset($request->comment) ? $request->comment : '',
				'opinion' => isset($request->opinion) ? $request->opinion : '',
				'params' => json_encode($params)
			)
		);

		$this->app->response()->status(201);
	}
}
?>