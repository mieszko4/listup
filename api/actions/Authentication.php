<?php

require_once('resources/Base.php');
require_once('../libs/facebook/facebook.php');

class Authentication extends Base {
	
	function post($LUPConfig) {
		$request=json_decode($this->app->request()->getBody());
	
		$_SESSION['type']=$request->type;
		
		if ($request->type == 'internal') {
			// check if user entered username or email
			$password=hash('sha256', $request->password);
			if (!filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
				$sth = $this->createSthAndExec('SELECT * FROM users WHERE username=? AND password=?', array($request->username, $password));
			} else {
				$sth = $this->createSthAndExec('SELECT * FROM users WHERE email=? AND password=?', array($request->username, $password));
			}
			
			if($user_data = $sth->fetch()) {
				$_SESSION['user_id']=$user_data['id'];
				
				//get avatar url
				$avatarUrl = file_exists("../client/avatars/{$user_data['id']}.jpg")?"../avatars/{$user_data['id']}.jpg":null;
				
				$this->app->response()->status(200);
				$this->app->response()->header('Content-Type', 'application/json');
				echo json_encode(array(
					"userID"	=>$user_data['id'],
					"type"		=>$request->type,
					"username"	=>$user_data['username'],
					"avatarUrl"	=>$avatarUrl
				));
			} else {
				if(isset($_SESSION['user_id'])) {
					session_destroy();
				}
				$this->app->response()->status(403);
				$this->app->response()->header('Content-Type', 'application/json');
				echo json_encode(array('Valid'=>'false', 'ErrorMessage'=>'Username/password not valid'));
			}
		} elseif ($request->type == 'facebook') {
			//check if user is valid using cookie
			$config = array();
			$config['appId'] = $LUPConfig->facebook->appId;
			$config['secret'] = $LUPConfig->facebook->secret;

			$facebook = new Facebook($config);
			$userId = $facebook->getUser();
			if ($userId) {
				try {
					$user_profile = $facebook->api('/me');
					
					$sth = $this->createSthAndExec('SELECT * FROM users WHERE userId=? AND `type`=?', array($userId, $request->type));
					
					$user = $sth->fetch();
					if(!$user) { //save user
						$sth = $this->createSthAndExec('INSERT INTO users(userId, `type`, username) VALUES (?, ?, ?)', 
										array($userId, $request->type, $user_profile['name']));
						
						$user = array();
						$user['id'] = $this->dbh->lastInsertId();
						$user['username'] = $user_profile['name'];
					}
					$user['avatarUrl'] = "http://graph.facebook.com/$userId/picture?type=square";
					
					$_SESSION['user_id']= $user['id'];
					
					$this->app->response()->status(200);
					$this->app->response()->header('Content-Type', 'application/json');
					
					echo json_encode(array(
						"userID"=>$user['id'],
						"type"=>$request->type,
						"username"=>$user['username'],
						"avatarUrl"=>$user['avatarUrl']
					));
				} catch (FacebookApiException $e) {
					$this->app->response()->status(403);
					$this->app->response()->header('X-Status-Reason', "Could not load facebook profile information");
				}
			} else {
				$this->app->response()->status(403);
				$this->app->response()->header('X-Status-Reason', "Facebook user not logged");
			}
		} elseif ($request->type == 'google+') {
			$type = $request->type;
			try {
				$json = file_get_contents($LUPConfig->$type->apiUrl."people/me?access_token=".$request->access_token);
				$user_profile = json_decode($json);
				
				if (!isset($user_profile->error)) {
					$sth = $this->createSthAndExec('SELECT * FROM users WHERE userId=? AND `type`=?',
									array($user_profile->id, $request->type));

					$user = $sth->fetch();
					if(!$user) { //save user
						$sth = $this->createSthAndExec('INSERT INTO users(userId, `type`, username) VALUES (?, ?, ?)',
										array($user_profile->id, $request->type, $user_profile->displayName));
						
						$user = array();
						$user['id'] = $this->dbh->lastInsertId();
						$user['username'] = $user_profile->displayName;
					}
					$user['avatarUrl'] = $user_profile->image->url;
					
					#update user info
					$params = isset($user['params'])?json_decode($user['params']):(new stdClass());
					$params->image = $user_profile->image;
					$sth = $this->createSthAndExec('UPDATE users SET params=? WHERE id=?', array(json_encode($params), $user['id']));
					
					$_SESSION['user_id']= $user['id'];
					
					$this->app->response()->status(200);
					$this->app->response()->header('Content-Type', 'application/json');
					
					echo json_encode(array(
						"userID"=>$user['id'],
						"type"=>$request->type,
						"username"=>$user['username'],
						"avatarUrl"=>$user['avatarUrl']
					));
				} else {
					$this->app->response()->status(403);
					$this->app->response()->header('X-Status-Reason', "1.Google+ user not logged");
				}
			} catch (Exception $e) {
				$this->app->response()->status(403);
				$this->app->response()->header('X-Status-Reason', "2.Google+ user not logged");
			}
		}
	}
	
	function get() {
		
		if(isset($_SESSION['user_id'])) {
			$sth = $this->createSthAndExec('SELECT `type`, userId, username, params FROM users WHERE id=?',
							array($_SESSION['user_id']));
			$result=$sth->fetch();
			
			//get avatar url
			$avatarUrl = null;
			if ($result['type'] == 'internal') {
				$avatarUrl = file_exists("../client/avatars/{$_SESSION['user_id']}.jpg")?"../avatars/{$_SESSION['user_id']}.jpg":null;
			} elseif ($result['type'] == 'facebook') {
				$avatarUrl = "http://graph.facebook.com/{$result['userId']}/picture?type=square";
			} elseif ($result['type'] == 'google+') {
				if (isset($result['params'])) {
					$params = json_decode($result['params']);
					if (isset($params->image)) {
						$avatarUrl = $params->image->url;
					}
				}
			}
			
			$this->app->response()->header('Content-Type', 'application/json');
			echo json_encode(array(
				"userId"=>$_SESSION['user_id'],
				"type"=>$result['type'],
				"username"=>$result['username'],
				"avatarUrl"=>$avatarUrl,
				"loggedIn"=>true
			));
		} else {
			$this->app->response()->status(200);
			$this->app->response()->header('Content-Type', 'application/json');
			echo json_encode(array("loggedIn"=>false));
		}
	}
	
	function logout() {
		session_destroy();
		$this->app->response()->status(204);
	}
	
}

?>