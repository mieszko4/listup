<?php

require_once('resources/Base.php');

class Fork extends Base {

	function post($id) {
		//check if playlist is visible
		$sth = $this->createSthAndExec('SELECT username, visibility, id_creator FROM playlists, users WHERE playlists.id=? AND playlists.id_creator=users.id',
				 array($id));
		
		$playlist = $sth->fetch();
		if (!$playlist) {
			throw new DataError('Playlist not found', 404);
		}

		if (!($playlist['visibility'] === 'all' || ($playlist['visibility'] === 'private' && $playlist['id_creator'] === $this->getUserId()))) {
			throw new DataError('You are not allowed to fork this playlist', 403);
		}

		$referer = $playlist['username'];

		// skopiraj playlistu
		$sth = $this->createSthAndExec('SELECT id, title, id_creator, imgURL, referer FROM playlists WHERE id=?',
				array($id));
		$forkedPlaylist = $sth->fetch(PDO::FETCH_ASSOC);
		
		$time = date('Y-m-d H:i:s');
		$sth = $this->createSthAndExec('INSERT INTO playlists(title, id_creator, imgURL, time, referer) VALUES(?, ?, ?, ?, ?)',
						array($forkedPlaylist['title'], $_SESSION['user_id'], $forkedPlaylist['imgURL'], $time, $referer));
		
		$playlistsID = $this->dbh->lastInsertId();
		
		// skopiraj pjesme
		$sth = $this->createSthAndExec('SELECT * FROM songs WHERE id_playlist=?', array($id));
		$songs = $sth->fetchAll();
		
		foreach($songs as $song) {
			$sth = $this->createSthAndExec('INSERT INTO songs(artist, title, id_playlist, search_term, imgURL, SCid) VALUES(?, ?, ?, ?, ?, ?)',
						array($song['artist'], $song['title'], $playlistsID, $song['search_term'], $song['imgURL'], $song['SCid']));
		}	

		# create notification
		try {
			$sth = $this->createSthAndExec('
				INSERT INTO notifications(user_id, referenced_playlist_id, is_new, type, datetime_created, datetime_modified) 
				VALUES (?, ?, ?, ?, NOW(), NOW())'
			,	array($this->getUserId(), $id, 1, 'fork'));		
		} catch (Exception $e) {
			// TODO: Log
		}
		#

		$this->app->response()->status(201);
	}	
}

?>
