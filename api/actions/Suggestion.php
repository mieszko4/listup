<?php

require_once('resources/Base.php');

class Suggestion extends Base {

	function accept_suggestion($id) {
		$request = json_decode($this->app->request()->getBody());
		
		// check if suggestion is ON 
		$sth = $this->createSthAndExec('SELECT suggest FROM playlists WHERE id=?', array($id));
		$suggest = $sth->fetch();
		if(!$suggest['suggest']) {
			throw new DataError('This playlist isnt opened for suggestions', 500);
		}
		
		// check owner
		$sth = $this->createSthAndExec('SELECT id FROM playlists WHERE id=? AND id_creator=?', array($id, $_SESSION['user_id']));
		$sth = $this->dbh->prepare('SELECT id FROM playlists WHERE id=? AND id_creator=?');
		if(!$sth->fetch()) {
			throw new DataError('You cannot accept suggestions on playlists that you dont own', 403);
		}
		
		// else set isAccepted to true
		$sth = $this->createSthAndExec('UPDATE songs SET isAccepted=true WHERE id=?', array($request->song_id));

		$this->app->response()->status(201);
	}

	function send_suggestion($id) {
		$request = json_decode($this->app->request()->getBody());
			
		//check if suggestion is ON and if it is visible
		$sth = $this->createSthAndExec('SELECT suggest, visibility, id_creator FROM playlists WHERE id=?', array($id));

		$playlist = $sth->fetch();
		if (!$playlist) {
			throw new DataError('Playlist not found', 404);
		}

		if (!($playlist['visibility'] === 'all' || ($playlist['visibility'] === 'private' && $playlist['id_creator'] === $this->getUserId()))) {
			throw new DataError('You are not allowed to fork this playlist', 403);
		}

		if(!$playlist['suggest']) {
			throw new DataError('This playlist isnt opened for suggestions', 500);
		}

		//else insert the suggestions into songs
		$accepted = false;
		$userId = $_SESSION['user_id'];
		foreach($request as $song) {
			$sth = $this->createSthAndExec('INSERT INTO songs(artist, title, id_playlist, search_term, imgURL, SCid, isAccepted, suggestor) VALUES(?, ?, ?, ?, ?, ?, ?, ?)',
							array($song->artist, $song->title, $id, $song->search_term, $song->imgURL, $song->SCid, $accepted, $userId));	
		} 
		$this->app->response()->status(201);
	}
}

?>