# Listup.audio #

Web application for collaborative music playlist creation and playing.

## Live page ##

Take a look at [live page](http://listup.audio/) and play around.

## How do I get set up? ##

* Configure nginx based on configurations/sample.nginx
* Run ```cp configurations/configuration.local.json configuration.json```
* Edit ```configuration.json``` with database parameters
* Import database schema from ```migrations/listup.sql``` then from ```migrations/update.sql``` 
* Edit ```configuration.json``` with facebook and googleplus 'login with' data
* Run ```npm install```
* Run ```grunt dev```
* The page is available on ```/index.dev.html```

## Automatic live playlist creation ##

In folder ```/live``` there is a script that can be used for automatic live playlists creation based on a source defined by a parser.

* Position yourself into ```/live``` folder
* Run ```npm install```
* Edit ```live.js``` and replace ```serviceUrl``` with url of the running page
* Edit ```live.js``` and replace ```username``` and ```password``` with any user credential that is registered on the running page
* Run ```node live.js $parserName $port```, i.e. ```node live.js "BBC Rock Singles" 80```
* A new playlist will be created
* You can add new source by defining new parser script in ```/parsers``` folder
