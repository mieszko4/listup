(function () {
	"use strict";
	/*jslint nomen: true, plusplus: true*/
	/*global Buffer: false, clearInterval: false, clearTimeout: false, console: false, exports: false, global: false, module: false, process: false, querystring: false, require: false, setInterval: false, setTimeout: false, __filename: false, __dirname: false */

	var request = require('request'),
		requestListup = request.defaults({jar: true}),
		fs = require('fs'),
		qs = require('querystring'),
		port = process.argv[3] || 82,
		serviceUrl = 'http://listup.audio:' + port + '/api/1.0/',
		username = 'listup',
		password = 'listup',
		scClientId = '6e17ff36dca5958058cb780d8d41cc99',
		ListUp = {
			login: function (success, args) {
				var requestBody = {
					'username': username,
					'type': 'internal',
					'password': password
				};

				requestListup.post({
					url: serviceUrl + 'login',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify(requestBody)
				}, function (error, serviceResponse, body) {
					var cookieParts;
					if (!error && serviceResponse.statusCode === 200) {
						success.apply(this, args);
					} else {
						console.error('Error while performing login: ', serviceResponse.status);
					}
				});
			},
			logout: function () {
				requestListup.post({
					url: serviceUrl + 'logout',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({})
				}, function (error, serviceResponse, body) {
					if (!error && (serviceResponse.statusCode.toString()).split('')[0] === '2') {
						console.log('Log out is done!', serviceResponse.statusCode);
					} else {
						console.error('Error while performing logout: ', serviceResponse.statusCode);
					}
				});
			}
		},
		parsers = {},
		getInfoFromSC = function (title, artist, success) {
			var serviceUrl = 'https://api.soundcloud.com/tracks',
				minLength = 10,
				rx = new RegExp('^(.{' + minLength + ',}?)\\s'),
				pTitle = title.match(rx),
				pArtist = artist.match(rx),
				params = {
					consumer_key: scClientId,
					filter: 'streamable',
					order: 'created_at',
					limit: '1'
				};

			//process title and artist
			title = (pTitle === null) ? title : pTitle[1];
			artist = (pArtist === null) ? artist : pArtist[1];
			params.q = artist + ' ' + title;

			request.get({
				url: serviceUrl + '?' + qs.encode(params),
				headers: {
					'Accept': 'application/json'
				}
			}, function (error, serviceResponse, body) {
				if (!error && serviceResponse.statusCode === 200) {
					var tracks,
						response = {};

					try {
						tracks = JSON.parse(body);
					} catch (e) {
					}

					if (tracks !== undefined && tracks.length > 0) {
						response = tracks[0];
					}
					success.call(this, response, params.q);
				} else {
					console.error('Error while getting playlist from listup: ', serviceResponse.statusCode);
				}
			});
		},
		updatePlaylist = function (playlist, playlistName, parserFunction) {
			var requestBody = playlist;

			parserFunction.call(this, function (songs) {
				var processedSongs = [],
					getAnotherSongInfo = function (songs) {
						var song = songs.shift();

						if (song !== undefined) {
							getInfoFromSC.call(this, song.title, song.artist, function (processedSong, query) {
								if (processedSong.id !== undefined) {
									processedSongs.push({
										artist: processedSong.user && processedSong.user.username,
										title: processedSong.title,
										search_term: 'from ' + playlistName + ': ' + query,
										imgURL: song.imgUrl || processedSong.artwork_url || './css/logoPlaylistGrey.png',
										SCid: processedSong.id,
										isAccepted: '1',
										suggestor: null,
										suggestor_name: null,
										order_position: processedSongs.length
									});
									console.log('Adding (artist, title):', processedSong.user && processedSong.user.username, processedSong.title, 'based on query:', query);
								} else {
									console.warn('Song was not found; skipping', song, 'with query:', query);
								}
								getAnotherSongInfo.call(this, songs);
							});
						} else {
							requestBody.songs = processedSongs;
							requestListup({
								method: (playlist.id !== undefined) ? 'PUT' : 'POST',
								url: (playlist.id !== undefined) ? (serviceUrl + 'playlists/' + playlist.id) : (serviceUrl + 'playlists'),
								headers: {
									'Content-Type': 'application/json'
								},
								body: JSON.stringify(requestBody)
							}, function (error, serviceResponse, body) {
								if (!error && (serviceResponse.statusCode.toString()).split('')[0] === '2') {
									console.log('DONE!');
									ListUp.logout();
								} else {
									console.error('Error while performing put: ', serviceResponse.statusCode, body);
								}
							});
						}
					};

				getAnotherSongInfo.call(this, songs);
			});
		},
		getPlaylist = function (id, playlistName, parserFunction) {
			var playlist;

			//get playlist
			request.get({
				url: serviceUrl + 'playlists/' + id,
				headers: {
					'X-Requested-With': 'XMLHttpRequest' //TODO: change in API
				}
			}, function (error, serviceResponse, body) {
				if (!error && serviceResponse.statusCode === 200) {
					try {
						playlist = JSON.parse(body);
					} catch (e) {
					}

					if (playlist !== undefined) {
						//console.log(playlist);
						updatePlaylist.call(this, playlist, playlistName, parserFunction);
					}
				} else {
					console.error('Error while getting playlist from listup: ', serviceResponse.statusCode);
				}
			});
		},
		getListUpPlaylist = function (playlistName, parserFunction) {
			var userId = 16; //TODO: hardcoded!, should be: "listup", change in API

			if (parserFunction === undefined) {
				console.error('Parser function is not defined for: ', playlistName);
				return;
			}

			//get playlists from creator
			request.get({
				url: serviceUrl + 'playlists?creator=' + userId
			}, function (error, serviceResponse, body) {
				var playlists = [],
					playlist,
					foundPlaylist;

				if (!error && serviceResponse.statusCode === 200) {
					try {
						playlists = JSON.parse(body);
					} catch (e) {
					}

					playlists.forEach(function (playlist) {
						if (playlist.title === playlistName) {
							foundPlaylist = playlist;
						}
					});

					if (foundPlaylist !== undefined) {
						getPlaylist.call(this, foundPlaylist.id, playlistName, parserFunction);
					} else {
						console.log('Playlist not found', 'will be created');
						playlist = {
							imgURL: "./css/logoPlaylistGreen.png",
							songs: [],
							suggest: 0,
							title: playlistName,
							vote: 0,
							visibility: 'all'
						};
						updatePlaylist.call(this, playlist, playlistName, parserFunction);
					}
				} else {
					console.error('Error while getting playlists from listup: ', serviceResponse.statusCode);
				}
			});
		};

	//start here
	if (process.argv[2] !== undefined) {
		//register parsers
		//each module in parsers folder must export function parse which, when parsing is done, must call its first argument, success, as "success.call(this, results)" where results is of the form [{artist: $artist, title: $title}, ...]
		fs.readdir('./parsers', function (error, filenames) {
			if (!error) {
				filenames.forEach(function (filename) {
					parsers[filename] = require('./parsers/' + filename).parse;
				});

				ListUp.login.call(this, getListUpPlaylist, [process.argv[2], parsers[process.argv[2] + '.js']]);
			}
		});
	} else {
		console.log('Usage: node live.js name');
	}
}());
