(function () {
	"use strict";
	/*jslint nomen: true*/
	/*global Buffer: false, clearInterval: false, clearTimeout: false, console: false, exports: false, global: false, module: false, process: false, querystring: false, require: false, setInterval: false, setTimeout: false, __filename: false, __dirname: false */

	var cheerio = require('cheerio'),
		request = require('request');

	exports.parse = function (success) {
		var serviceUrl = 'http://www.bbc.co.uk/radio1/chart/singles',
			handleResponse = function (document, success) {
				var $ = cheerio.load(document),
					$entries = $('.cht-entries').children(),
					results = [];

				if ($entries.length !== 0) {
					console.log('found');

					$entries.each(function () {

						var $entry = this.find(''),
							$songTitle = this.children().children()[2].children[2].next.children[0].data,
							img = this.children().children()[1].attribs.src,
							artist = this.children().children()[2].children[0].next.children[0].data,
							title = this.children().children()[2].children[2].next.children[0].data;

						results.push({
							'artist': artist,
							'title': title,
							'imgUrl': img
						});

					});
				} else {
					console.log('not found');
				}

				success.call(this, results);
			};

		request.get({
			url: serviceUrl
		}, function (error, serviceResponse, body) {
			if (!error && serviceResponse.statusCode === 200) {
				handleResponse.call(this, body, success);
			} else {
				console.error('Error while getting playlist from otvoreni: ', serviceResponse.statusCode);
			}
		});
	};
}());