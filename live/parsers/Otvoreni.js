(function () {
	"use strict";
	/*jslint nomen: true*/
	/*global Buffer: false, clearInterval: false, clearTimeout: false, console: false, exports: false, global: false, module: false, process: false, querystring: false, require: false, setInterval: false, setTimeout: false, __filename: false, __dirname: false */

	var cheerio = require('cheerio'),
		request = require('request');

	exports.parse = function (success) {
		var serviceUrl = 'http://otvoreni.hr/default.aspx?id=12',
			handleResponse = function (document, success) {
				var $ = cheerio.load(document),
					$table = $('.pageContent table'),
					$trs,
					results = [];

				if ($table.length !== 0) {
					console.log('found');

					$trs = $table.find('tr');
					$trs.each(function () {
						var $td = this.find('td:not(.onairtd)'),
							$songTitle = $td.eq(0),
							time = $td.eq(1).text(),
							artist,
							title;

						if ($songTitle.length > 0) {
							title = $songTitle.find('b').text();

							$songTitle.find('b').remove();
							artist = $songTitle.text();

							results.push({
								'artist': artist,
								'title': title,
								'time': time
							});
						}
					});
				} else {
					console.log('not found');
				}

				success.call(this, results);
			};

		request.get({
			url: serviceUrl
		}, function (error, serviceResponse, body) {
			if (!error && serviceResponse.statusCode === 200) {
				handleResponse.call(this, body, success);
			} else {
				console.error('Error while getting playlist from otvoreni: ', serviceResponse.statusCode);
			}
		});
	};
}());