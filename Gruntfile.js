module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: ';\n'
			},
			js: {
				src: [
					'client/js/app.js',
					'client/js/controllers/*.js',
					'client/js/filters.js',
					'client/js/services.js',
					'client/js/directives.js',
					'client/js/analytics.js',
					'client/js/facebook.js',
					'client/js/google.js',
					'client/js/http-error-interceptor.js'
				],
				dest: 'client/js/<%= pkg.name %>.js'
			},
			lib: {
				src: [
					'client/lib/jquery.js',
					'client/js/transition.js',
					'client/lib/jquery-ui.js',
					'client/lib/jquery.mobile.js',
					'client/lib/jquery.ui.touch-punch.js',
					'client/lib/angular/angular.js',
					'client/lib/jquery-mobile-angular-adapter.js',
					'client/lib/angular/angular-resource.js',
					'client/lib/http-auth-interceptor.js',
					'client/lib/ui-sortable.js',
					'client/lib/infinite-scroll.js'
				],
				dest: 'client/lib/<%= pkg.name %>.js'
			}
		},
		ngmin: {
			js: {
				src: ['client/js/<%= pkg.name %>.js'],
				dest: 'client/js/<%= pkg.name %>.ngmin.js'
			}
		},
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			js: {
				src: 'client/js/<%= pkg.name %>.ngmin.js',
				dest: 'client/js/<%= pkg.name %>.min.js'
			},
			lib: {
				src: 'client/lib/<%= pkg.name %>.js',
				dest: 'client/lib/<%= pkg.name %>.min.js'
			}
		},
		cssmin: {
			all: {
				src: ['client/css/listup.css'],
				dest: 'client/css/<%= pkg.name %>.min.css'
			}
		},
		clean: {
			prod: {
				src: [
					'client/lib/*',
					'!client/lib/listup.min.js',
					'client/js/*',
					'!client/js/listup.min.js',
					'client/css/**/*.css',
					'!client/css/listup.min.css',
					'client/index.dev.html'
				]
			},
			dev: {
				src: [
					'client/lib/listup.js',
					'client/lib/listup.min.js',
					'client/js/listup.js',
					'client/js/listup.ngmin.js',
					'client/js/listup.min.js',
					'client/css/listup.min.css',
					'client/index.html'
				]
			}
		},
		copy: {
			prod: {
				src: ['configurations/configuration.server.json'],
				dest: 'configuration.json'
			},
			dev: {
				src: ['configurations/configuration.server.dev.json'],
				dest: 'configuration.json'
			}
		},
		replace: {
			cssjs: {
				src: ['client/index.html'],
				overwrite: true,
				replacements: [
					{
						from: /(src|href)="(.+\.min\.[^?"]+)"/g,
						to: function (w, p, c, groups) {
							var crypto = require('crypto'),
								fingerprint = crypto.createHash('md5'),
								filename = groups[1],
								content = grunt.file.read('client/' + filename);

							fingerprint.update(content);

							return groups[0] + '="' + filename + '?h=' + fingerprint.digest('hex') + '"';
						}
					}
				]
			},
			partials: {
				src: ['client/js/app.js'],
				overwrite: true,
				replacements: [
					{
						from: /templateUrl[^:]*:[^'"]*(?:'|")([^?'"]+)(?:'|")/g,
						to: function (w, p, c, groups) {
							var crypto = require('crypto'),
								fingerprint = crypto.createHash('md5'),
								filename = groups[0],
								content = grunt.file.read('client/' + filename);

							fingerprint.update(content);

							return "templateUrl: '" + filename + '?h=' + fingerprint.digest('hex') + "'";
						}
					}
				]
			},
			includes: { //Note: Works only for partials (not recursive)
				src: ['client/partials/*.html'],
				overwrite: true,
				replacements: [
					{
						from: /ng-include="'([^?']+)'"/g,
						to: function (w, p, c, groups) {
							var crypto = require('crypto'),
								fingerprint = crypto.createHash('md5'),
								filename = groups[0],
								content = grunt.file.read('client/' + filename);

							fingerprint.update(content);

							return 'ng-include="\'' + filename + '?h=' + fingerprint.digest('hex') + '\'"';
						}
					}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-ngmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-text-replace');

	grunt.registerTask('default', ['dev']);
	grunt.registerTask('min-js', ['concat:js', 'ngmin:js', 'uglify:js', 'cssmin']);
	grunt.registerTask('min-all', ['concat', 'ngmin', 'uglify', 'cssmin']);
	grunt.registerTask('prod', ['replace:includes', 'replace:partials', 'min-all', 'replace:cssjs', 'clean:prod', 'copy:prod']);
	grunt.registerTask('dev', ['clean:dev', 'copy:dev']);
};
