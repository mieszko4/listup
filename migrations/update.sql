ALTER TABLE `users`
	ADD `type` ENUM('internal', 'facebook', 'google+') NOT NULL DEFAULT 'internal' AFTER `id`,
	ADD `userId` VARCHAR(32) NULL DEFAULT NULL AFTER `id`,
	CHANGE `email` `email` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	CHANGE `password` `password` CHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	ADD `params` TEXT NOT NULL
;

ALTER TABLE `songs`
	ADD `suggestor` int(11) DEFAULT NULL
;


# 20130928 - Miroslav

ALTER TABLE `songs`
        ADD `order_position` int(11) DEFAULT NULL;


ALTER TABLE `playlists`
	ADD  `visibility` ENUM('all', 'friends', 'private') NOT NULL DEFAULT 'all'
;

# 20131010 - Miroslav

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `referenced_playlist_id` int(11) NOT NULL,
  `is_new` int(11) NOT NULL,
  `type` enum('suggestion','fork') NOT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `datetime_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=1 ;

ALTER TABLE `feedback`
	ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD `params` TEXT NULL
;