-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2013 at 03:00 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `listup`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `opinion` tinyint(1) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `opinion`, `comment`) VALUES
(7, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id_playlist` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `id_creator` int(11) NOT NULL,
  `imgURL` varchar(256) DEFAULT NULL,
  `time` datetime NOT NULL,
  `vote` tinyint(1) NOT NULL DEFAULT '0',
  `suggest` tinyint(1) NOT NULL DEFAULT '0',
  `referer` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `playlists_ibfk_1` (`id_creator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=128 ;

--
-- Dumping data for table `playlists`
--

INSERT INTO `playlists` (`id`, `title`, `id_creator`, `imgURL`, `time`, `vote`, `suggest`, `referer`) VALUES
(109, 'Lejm', 6, './css/logoPlaylistGrey.png', '2013-05-12 19:56:58', 0, 0, NULL),
(111, 'test', 6, 'http://i1.sndcdn.com/artworks-000040515736-nvxi2s-large.jpg?9556ac0', '2013-05-13 01:08:59', 0, 1, NULL),
(114, 'nickelback', 6, 'http://i1.sndcdn.com/artworks-000011887046-gsw3jt-large.jpg?9556ac0', '2013-05-11 15:25:18', 0, 0, NULL),
(115, 'Svašta', 8, './css/logoPlaylistGrey.png', '2013-05-11 20:31:18', 0, 0, NULL),
(118, 'Žešće', 8, 'http://i1.sndcdn.com/artworks-000009165918-vbsfma-large.jpg?9556ac0', '2013-05-11 21:35:46', 0, 1, NULL),
(123, 'nickelback', 8, 'http://i1.sndcdn.com/artworks-000011887046-gsw3jt-large.jpg?9556ac0', '2013-05-12 21:17:21', 0, 0, 'eni'),
(124, 'neka pleylista', 6, 'http://i1.sndcdn.com/artworks-000001413469-vn5a3r-large.jpg?9556ac0', '2013-05-12 22:13:16', 0, 0, NULL),
(125, 'NOVO', 6, './css/logoPlaylistGrey.png', '2013-05-13 00:56:55', 0, 0, NULL),
(126, 'Žešće', 6, 'http://i1.sndcdn.com/artworks-000009165918-vbsfma-large.jpg?9556ac0', '2013-05-13 03:13:00', 0, 0, 'martina'),
(127, 'Žešće', 10, 'http://i1.sndcdn.com/artworks-000009165918-vbsfma-large.jpg?9556ac0', '2013-05-13 03:14:47', 0, 0, 'eni');

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE IF NOT EXISTS `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(63) NOT NULL,
  `title` varchar(63) NOT NULL,
  `id_playlist` int(11) NOT NULL,
  `search_term` varchar(63) NOT NULL,
  `imgURL` varchar(64) DEFAULT NULL,
  `SCid` varchar(32) DEFAULT NULL,
  `isAccepted` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_playlist` (`id_playlist`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=417 ;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `artist`, `title`, `id_playlist`, `search_term`, `imgURL`, `SCid`, `isAccepted`) VALUES
(327, 'Nickelback', 'When We Stand Together', 114, 'nickelback', 'http://i1.sndcdn.com/artworks-000011887046-gsw3jt-large.jpg?9556', '23935038', 1),
(330, 'vytaandri14', 'Justin Timberlake - Mirrors', 115, 'timberlake mirrors', 'http://i1.sndcdn.com/artworks-000044868735-jmovcq-large.jpg?9556', '86737921', 1),
(331, 'Rihanna', 'Diamonds Remix f/ Kanye West', 115, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 1),
(332, '50 Cent', '50 Cent - My Life ft. Eminem & Adam Levine (OUT NOW)', 115, 'eminem', 'http://i1.sndcdn.com/artworks-000034960648-2doqjp-large.jpg?9556', '68924820', 1),
(333, 'kitzia1', 'Backstreet Boys - Show Me The Meaning Of Being Lonely', 115, 'backstreet boys', './css/logoPlaylistGrey.png', '13048339', 1),
(338, 'YangBanGaMoon', 'Nickelback - Someday', 118, 'nickelback someday', './css/logoPlaylistGrey.png', '22964038', 1),
(339, 'Cody Edie', 'Linking Park In The End Cover', 118, 'linking park in the end', './css/logoPlaylistGrey.png', '28129916', 1),
(340, 'ThisisCharlie', 'Lips of an angel- Hinder', 118, 'lips of an angel', 'http://i1.sndcdn.com/artworks-000018802566-uplzlw-large.jpg?9556', '19394089', 1),
(341, 'Karla Molkovich', 'Chop Suey (System of a Down)', 118, 'sistem of a down spiders', 'http://i1.sndcdn.com/artworks-000035925214-b6w69h-large.jpg?9556', '67575220', 1),
(342, 'HamNia', 'Evanescence - My Immortal', 118, 'evanescence my immortal', 'http://i1.sndcdn.com/artworks-000028568661-nx743w-large.jpg?9556', '56506318', 1),
(346, 'Iwonacomet', 'Red Hot Chilli Peppers - Snow', 109, 'red hot chilli peppers snow', './css/logoPlaylistGrey.png', '64174643', 1),
(347, 'user868980442', 'The Beatles - Hey Jude', 109, 'the beatles hey jude', './css/logoPlaylistGrey.png', '63781753', 1),
(349, 'Magdaléna Gray', 'The Rolling Stones- Angie', 109, 'the rolling stones angie', './css/logoPlaylistGrey.png', '16707233', 1),
(369, 'Arman Sehic', 'Dino Merlin -  Lazu me', 109, 'lažu me dino merlin', './css/logoPlaylistGrey.png', '83277383', 1),
(373, 'Rihanna', 'Pour It Up Remix', 109, 'rihanna', 'http://i1.sndcdn.com/artworks-000043421965-cui579-large.jpg?9556', '84117195', 1),
(374, 'Justin Timberlake', 'Justin Timberlake - Suit & Tie Featuring JAY Z (30 Sec Preview)', 109, 'timberlake', 'http://i1.sndcdn.com/artworks-000039185136-u6hqt3-large.jpg?9556', '76476435', 1),
(376, 'Conor Black 1', 'Here Comes The Sun', 109, '', './css/logoPlaylistGrey.png', '91847732', 1),
(378, 'Severina', 'Running out', 109, 'severina', 'http://i1.sndcdn.com/artworks-000014715332-4o0756-large.jpg?9556', '29355900', 1),
(379, 'LateNite Works', 'Ana Nikolic - Romale Romali 2011 Late Night DJ''s & LNW original', 109, 'romale romali', './css/logoPlaylistGrey.png', '30441180', 1),
(380, 'Severina', 'Doubt of the deep', 109, 'severina', 'http://i1.sndcdn.com/artworks-000012541338-644wvc-large.jpg?9556', '25154765', 1),
(381, 'Severina', 'Doubt of the deep', 109, 'severina', 'http://i1.sndcdn.com/artworks-000012541338-644wvc-large.jpg?9556', '25154765', 1),
(382, 'kitzia1', 'Backstreet Boys - Show Me The Meaning Of Being Lonely', 109, 'backstreet boys', './css/logoPlaylistGrey.png', '13048339', 1),
(383, 'Rihanna', 'Pour It Up Remix', 109, 'rihanna', 'http://i1.sndcdn.com/artworks-000043421965-cui579-large.jpg?9556', '84117195', 1),
(384, 'Mitja Vrhovnik Smrekar', 'Josipa Lisac - Sirakuza', 109, 'josipa lisac', './css/logoPlaylistGrey.png', '74703686', 1),
(385, 'GrunerAlter', 'Dubioza Kolektiv - Balkan Funk', 109, 'dubioza kolektiv', 'http://i1.sndcdn.com/artworks-000004697579-ndfbyq-large.jpg?9556', '10252786', 1),
(386, 'Rihanna', 'Diamonds Remix f/ Kanye West', 109, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 0),
(387, 'Marius Hoersturz', 'Marius Hörsturz - Umbrella', 109, 'rihanna umbrella', 'http://i1.sndcdn.com/artworks-000028570649-k3hzae-large.jpg?9556', '56510377', 1),
(388, 'Nickelback', 'When We Stand Together', 123, 'nickelback', 'http://i1.sndcdn.com/artworks-000011887046-gsw3jt-large.jpg?9556', '23935038', 1),
(390, 'Rihanna', 'Pour It Up Remix', 124, 'rihanna', 'http://i1.sndcdn.com/artworks-000043421965-cui579-large.jpg?9556', '84117195', 1),
(391, 'Justin Timberlake', 'Justin Timberlake - Suit & Tie Featuring JAY Z (30 Sec Preview)', 124, 'timberlake', 'http://i1.sndcdn.com/artworks-000039185136-u6hqt3-large.jpg?9556', '76476435', 1),
(392, 'LoveAvalanche/Al Lindrum', 'Dubioza Kolektiv "Kokuz" Al Lindrum`s Bosnian Beans Dub', 124, 'dubioza kolektiv kokuz', 'http://i1.sndcdn.com/artworks-000001413469-vn5a3r-large.jpg?9556', '2864951', 1),
(398, 'Rihanna', 'Diamonds Remix f/ Kanye West', 111, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 0),
(399, 'DjC3', 'Mc Vn - Vuco Vuco Dj C3 Barracão da Villa', 111, 'vuco', './css/logoPlaylistGrey.png', '84957778', 0),
(400, 'Rihanna', 'Diamonds Remix f/ Kanye West', 111, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 0),
(401, 'Rihanna', 'Diamonds Remix f/ Kanye West', 125, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 1),
(402, 'Blackkartel Entertainment', 'Justin Timberlake - Mirrors - www.blackkartel.com/Hot_New_Rnb_ ', 125, 'timberlake', 'http://i1.sndcdn.com/artworks-000040515736-nvxi2s-large.jpg?9556', '78876687', 1),
(403, 'DjC3', 'Mc Vn - Vuco Vuco Dj C3 Barracão da Villa', 125, 'vuco', './css/logoPlaylistGrey.png', '84957778', 1),
(404, 'Nina Viskovic', 'Vlado Kalember Vino na usnama', 111, 'vlado kalember', './css/logoPlaylistGrey.png', '53994273', 0),
(405, 'Rihanna', 'Diamonds Remix f/ Kanye West', 111, 'rihanna', 'http://i1.sndcdn.com/artworks-000034315485-g1z6sf-large.jpg?9556', '67658191', 1),
(406, 'Blackkartel Entertainment', 'Justin Timberlake - Mirrors - www.blackkartel.com/Hot_New_Rnb_ ', 111, 'timberlake', 'http://i1.sndcdn.com/artworks-000040515736-nvxi2s-large.jpg?9556', '78876687', 1),
(407, 'YangBanGaMoon', 'Nickelback - Someday', 126, 'nickelback someday', './css/logoPlaylistGrey.png', '22964038', 1),
(408, 'Cody Edie', 'Linking Park In The End Cover', 126, 'linking park in the end', './css/logoPlaylistGrey.png', '28129916', 1),
(409, 'ThisisCharlie', 'Lips of an angel- Hinder', 126, 'lips of an angel', 'http://i1.sndcdn.com/artworks-000018802566-uplzlw-large.jpg?9556', '19394089', 1),
(410, 'Karla Molkovich', 'Chop Suey (System of a Down)', 126, 'sistem of a down spiders', 'http://i1.sndcdn.com/artworks-000035925214-b6w69h-large.jpg?9556', '67575220', 1),
(411, 'HamNia', 'Evanescence - My Immortal', 126, 'evanescence my immortal', 'http://i1.sndcdn.com/artworks-000028568661-nx743w-large.jpg?9556', '56506318', 1),
(412, 'YangBanGaMoon', 'Nickelback - Someday', 127, 'nickelback someday', './css/logoPlaylistGrey.png', '22964038', 1),
(413, 'Cody Edie', 'Linking Park In The End Cover', 127, 'linking park in the end', './css/logoPlaylistGrey.png', '28129916', 1),
(414, 'ThisisCharlie', 'Lips of an angel- Hinder', 127, 'lips of an angel', 'http://i1.sndcdn.com/artworks-000018802566-uplzlw-large.jpg?9556', '19394089', 1),
(415, 'Karla Molkovich', 'Chop Suey (System of a Down)', 127, 'sistem of a down spiders', 'http://i1.sndcdn.com/artworks-000035925214-b6w69h-large.jpg?9556', '67575220', 1),
(416, 'HamNia', 'Evanescence - My Immortal', 127, 'evanescence my immortal', 'http://i1.sndcdn.com/artworks-000028568661-nx743w-large.jpg?9556', '56506318', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `email` varchar(31) NOT NULL,
  `password` char(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(6, 'eni', 'eni@listup.audio', '4d4f0f7a97c8f403595253ab253eafe715b67b73c2906b068f2cb99365698e53'),
(8, 'martina', 'martina@listup.audio', '16b02287e8a9f1d5bb5a4afacaf6ac7124c62846bad58d8f48acc27598a2cb7a'),
(9, 'anja', 'anja@gmail.com', '0fd685b4f9210dc381240c7ff26f3e4298f38504753c0b7bb0200560f1cd820d'),
(10, 'milosz', 'milosz@listup.audio', '77ffe61393e9a559ebd71c176e97d02a920a9202703b15970a7b7dff445160eb'),
(13, 'tea', 'tea@gmail.com', 'a9f74d1ec36ebdeb2da3f6e5868090cd2a2d20b3dcca7b62f60304b1d3d9ef42'),
(14, 'ivana', 'ivana@gmail.com', 'feee3235626c079da26ff0ccbacba430f809cdf5a8dc7358aee52f05ae2e6ed0'),
(15, 'trina', 'trina@gma.com', '886ba5adc67d61235fd73e4bbf93fb1b7bb435d47aca26f424d08d1c863ec6cc');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id_song` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `playlists_ibfk_1` FOREIGN KEY (`id_creator`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_ibfk_1` FOREIGN KEY (`id_playlist`) REFERENCES `playlists` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
